package hja;

import java.util.ArrayList;

import hja.model.Carta;
import hja.model.Comparador;
import hja.model.Jugada;
import hja.model.Posicion;
import hja.newGui.PanelJuego;

public class Juego {
	
	private static final int BB = 2;
	private Jugada actual;
	private int turno=0;
	private int nJugadores=4;
	private boolean end;
	private int bote=0;
	private Comparador c;
	public Juego(){
		c=new Comparador(new ArrayList<Carta>(),new ArrayList<Carta>());
		actual=c.jugadaAleatoria();//por terminar
		end=false;
	}
	
	public void inicio(){
		end = false;
		while (nJugadores>1 || !end || true){
			actual = c.jugadaAleatoria();
			boolean or=false;
			PanelJuego.mostrarCartas(actual.getJugadores()[3], null, null, null, 0);
			PanelJuego.mostrarBoard(null, null, null, null, null);
			for (int i=0;i<nJugadores;i++){
				
				bote=3;				
				Posicion raise=null;
				actual.getJugadores()[(i+turno)%nJugadores].menos(BB/2);
				actual.getJugadores()[(i+turno+1)%nJugadores].menos(BB);
				if (i+turno%nJugadores==3){
					while(!PanelJuego.getDecision()){
						
					}
					if(!or) {
						if(PanelJuego.getRaise()) {
							or=true;
							bote+=3*BB;
							raise=Posicion.values()[i+turno%nJugadores];
							actual.getJugadores()[(i+turno+1)%nJugadores].menos(3*BB);
						} else if(PanelJuego.getFold()) {
							
						}
					} else {if (PanelJuego.getCall()) {
							if (Posicion.values()[i+turno%nJugadores]==Posicion.CO || Posicion.values()[i+turno%nJugadores] == Posicion.BTN){
								
								bote+=3*BB;
								actual.getJugadores()[(i+turno+1)%nJugadores].menos(3*BB);
							}
							else if (Posicion.values()[i+turno%nJugadores]==Posicion.SB){
								bote+=2*BB+1;
							}
							else{
								bote+= 2*BB;
								actual.getJugadores()[(i+turno+1)%nJugadores].menos(2*BB);
							}
								
						} else if (PanelJuego.getFold()) {
							actual.borrarJugador(i);
						}
					}
				}else{
					if (!or){
						if (Accion.RAISE==actual.relacionarseConBote(null,Posicion.values()[i+turno%nJugadores] , Accion.CHECK, i)){
							or=true;
							bote+=3*BB;
							raise=Posicion.values()[i+turno%nJugadores];
						}
					}else if (Accion.CALL==actual.relacionarseConBote(raise,Posicion.values()[i+turno%nJugadores] , Accion.CHECK, i)){
						if (Posicion.values()[i+turno%nJugadores]==Posicion.CO || Posicion.values()[i+turno%nJugadores] == Posicion.BTN)
							bote+=3*BB;
						else if (Posicion.values()[i+turno%nJugadores]==Posicion.SB)
							bote+=2*BB+1;
						else
							bote+= 2*BB;
					}else
						actual.borrarJugador(i);
				}
			}
			//jugador juega TODO
			
				
			PanelJuego.mostrarBoard(actual.getMesa().getCartas()[0], actual.getMesa().getCartas()[1], actual.getMesa().getCartas()[2], actual.getMesa().getCartas()[3], actual.getMesa().getCartas()[4]);
			PanelJuego.mostrarCartas(null, actual.getJugadores()[0], actual.getJugadores()[1], actual.getJugadores()[2], 1);
			PanelJuego.actualizarBotes(actual.getJugadores()[3].getDinero(), actual.getJugadores()[0].getDinero(), actual.getJugadores()[1].getDinero(), actual.getJugadores()[2].getDinero());
			ArrayList<Integer>ganadores = actual.getIdGanadores();
			for(int id : ganadores)
				actual.getJugadores()[id].mas(bote/ganadores.size());
			PanelJuego.actualizarBotes(actual.getJugadores()[3].getDinero(), actual.getJugadores()[0].getDinero(), actual.getJugadores()[1].getDinero(), actual.getJugadores()[2].getDinero());
			siguienteTurno();
			bote=0;
		}
	}
	
	
	private void siguienteTurno(){
		turno++;
	}
}
