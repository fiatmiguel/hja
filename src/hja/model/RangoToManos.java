package hja.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class RangoToManos {
	private static ArrayList<String> tipos = new ArrayList<String>();
	private static String[][] rangos = new String[1][169];
	public static void descarga() {
		FileWriter f = null;
		try {
			f = new FileWriter("entrada.txt");
		} catch (IOException e) {
			
		}
		BufferedWriter b = new BufferedWriter(f);
		for (int i = 0; i < tipos.size(); i++) {
			try {
				b.write(tipos.get(i));
			} catch (IOException e) {
				
			}
		}
		for (int i = 0; i < tipos.size(); i++) {
			try {
				f = new FileWriter(tipos.get(i));
			} catch (IOException e) {
				
			}
			b = new BufferedWriter(f);
			for (int j = 0; j < 169; j++) {
				try {
					b.write(rangos[i][j]);
				} catch (IOException e) {
	
				}
				
			}
			try {
				b.write("-1");
			} catch (IOException e) {
				
			}
		}
	}
	public static ArrayList<String> cargaInicial() {
		FileReader f = null;
		try {
			f = new FileReader("entrada.txt");
		} catch (FileNotFoundException e) {
			
		}
		BufferedReader b = new BufferedReader(f);
		String cadena = null;
		try {
			cadena = b.readLine();
		} catch (IOException e) {
			
		}
		while (!cadena.equalsIgnoreCase("-1")) {
			carga(cadena);
			try {
				cadena = b.readLine();
			} catch (IOException e) {
				
			}
		}
		return tipos;
	}
	public static ArrayList<String> carga(String entrada) {
		tipos.add(entrada);
		if (tipos.size() != 1) {
			String[][] aux = new String[tipos.size()][169];
			for (int i = 0; i < tipos.size() - 1; i++) {
				for (int j = 0; j < 169; j++) {
					aux[i][j] = rangos[i][j];
				}
			}
			rangos = aux;
		}
		
		FileReader f = null;
		try {
			f = new FileReader(entrada + ".txt");
		} catch (FileNotFoundException e) {
			System.out.println("archivo malo " + entrada);
		}
		BufferedReader b = new BufferedReader(f);
		String cadena = null;
		try {
			cadena = b.readLine();
		} catch (IOException e) {
			
		}
		int w = 0;
		while (cadena != "-1" && w < 169) {
			if (cadena.charAt(0) == cadena.charAt(1) || cadena.length() == 3) {
				rangos[tipos.size() - 1][w] = cadena;
			} else {
				rangos[tipos.size() - 1][w] = cadena + "o";
			}	
			try {
				cadena = b.readLine();
			} catch (IOException e) {
				
			}
			w++;
		}
		return tipos;
		
	}
	public static boolean[][] ToManos(int rango, String tipo) {
		boolean[][] manos = new boolean[13][13];
		int numManos = (rango * 169) / 100, x, y;
		for (int i = 0; i < tipos.size(); i++) {
			if (tipos.get(i).equalsIgnoreCase(tipo)) {
				for (int j = 0; j < numManos; j++) {
					x = ParserRango.cardToInt(rangos[i][j].charAt(0));
					y = ParserRango.cardToInt(rangos[i][j].charAt(1));
					if (rangos[i][j].length() == 2) {				
						manos[y][x] = true;
					} else if (rangos[i][j].length() == 3) {
						if (rangos[i][j].charAt(2) == 'o') {
							
							manos[y][x] = true;
						} else if (rangos[i][j].charAt(2) == 's') {
							manos[x][y] = true;
						} 				
					}
				}
			}
		}
		
		return manos;
	}	
	
}
