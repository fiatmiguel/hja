package hja.model;

import java.util.ArrayList;

public class ParserRango {
	public static ArrayList<Carta> parseadorCartas(String cadena) {
		ArrayList<Carta> cartas = new ArrayList<Carta>();
		if (!cadena.equals("")) {
			String[] s = cadena.split(",");
			for (int i = 0; i < s.length; i++) {
				cartas.add(new Carta(cardToPalo(s[i].charAt(1)), cardToValor(s[i].charAt(0))));
			}
		}
		
		
		return cartas;		
	}
	public static boolean[][] parseadorRango(String cadena, ArrayList<Mano> manos, int nJugador) {
		boolean[][] rango = new boolean[13][13];
		String[] s = cadena.split(",");
		int x , y;
		if (s[0].equalsIgnoreCase("random")) {
			for (int i = 0; i < 13; i++) {
				for (int j = 0; j < 13; j++) {
					rango[i][j] = true;
				}
			}
		} else {
			for (int i = 0; i < s.length; i++) {
				x = cardToInt(s[i].charAt(0));
				y = cardToInt(s[i].charAt(1));
				if (s[i].length() == 2) {
					rango[y][x] = true;
				} else if (s[i].length() == 3) {
					if (s[i].charAt(2) == '+') {
						int w = x, j = y;
						while (w >= 0 && j >= 0) {
							rango[w][j] = true;
							w--;
							j--;
						}
					} else if (s[i].charAt(2) == 's') {
						rango[x][y] = true;
					} else {
						rango[y][x] = true;				
					}
					
				} else if (s[i].length() == 4) {
					if (s[i].charAt(3) == '+') {
						if (s[i].charAt(2) == 's') {
							int j = y;
							while (x < j) {
								rango[x][j] = true;
								j--;
							}
						} else {
							int w = y;
							while (w > x) {
								rango[w][x] = true;
								w--;
							}
						}
					} else {
						// TODO crear las manos con una id que es el nJugador
						manos.add(new Mano(new Carta(cardToPalo(s[i].charAt(1)), cardToValor(s[i].charAt(0))), new Carta(cardToPalo(s[i].charAt(3)),cardToValor(s[i].charAt(2))), nJugador));
					}
					
				} else if (s[i].length() == 5) {
					int w = cardToInt(s[i].charAt(3)), z = cardToInt(s[i].charAt(4));
					while (x <= z) {
						rango[w][z] = true;
						w--;
						z--;
					}
					
				} else if (s[i].length() == 7) {
					int z = cardToInt(s[i].charAt(5));
					if (s[i].charAt(6) == 's') {
						while (y <= z) {
							rango[x][z] = true;
							z--;
						}
					} else {
						while (y <= z) {
							rango[z][x] = true;
							z--;
						}
					}
				}
			}
		}
		
		return rango;
	}
	public static int cardToInt(char c) {
		int v = 14;
		switch(c) {
			case 'A': v = 0; break;
			case '2': v = 12; break;
			case '3': v = 11; break;
			case '4': v = 10; break;
			case '5': v = 9; break;
			case '6': v = 8; break;
			case '7': v = 7; break;
			case '8': v = 6; break;
			case '9': v = 5; break;
			case 'T': v = 4; break;
			case 'J': v = 3; break;
			case 'Q': v = 2; break;
			case 'K': v = 1; break;
		}
		return v;
	}
	public static Palo cardToPalo(char c) {
		Palo v = null;
		switch(c) {
			case 'h': v = Palo.heart; break;
			case 's': v = Palo.spade; break;
			case 'd': v = Palo.diamonds; break;
			case 'c': v = Palo.clover; break;
		}
		return v;
	}
	public static Valor cardToValor(char c) {
		Valor v = null;
		switch(c) {
			case 'A': return Valor.A; 
			case '2': return Valor.two; 
			case '3': return Valor.three; 
			case '4': return Valor.four;
			case '5': return Valor.five;
			case '6': return Valor.six;
			case '7': return Valor.seven;
			case '8': return Valor.eight;
			case '9': return Valor.nine;
			case 'T': return Valor.T;
			case 'J': return Valor.J;
			case 'Q': return Valor.Q;
			case 'K': return Valor.K;
		}
		return v;
	}
}
