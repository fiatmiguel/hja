package hja.model;

import java.util.ArrayList;

import hja.Accion;
import hja.Bot;

public class Jugada {
	private Mano[] jugadores;
	private int nJugadores;
	private Mesa mesa;
	private int  nGanadores=1;
	ArrayList<Baza> bazas;
	Bot bot = new Bot();
	public final int BB = 2;
	
	public Jugada(Mano[] manos, int jugadores, Mesa mesa) {
		nJugadores = jugadores;
		this.jugadores = new Mano[nJugadores];
		this.jugadores = manos;
		this.mesa = mesa;
	}
	
	public void borrarJugador(int x){
		jugadores[x].abandona();
	}
	
	public Accion relacionarseConBote(Posicion posicion0, Posicion posicion1, Accion action, int id){
		Accion accion=null;
		if (action==Accion.CHECK){
			if ( bot.decidirOpenRaise(jugadores[id].toRank(), posicion1))
			accion = Accion.RAISE;
			jugadores[id].menos(3*BB);
		}else if (action==Accion.RAISE){
			if(posicion1==Posicion.BTN || posicion1==Posicion.CO){
				if (bot.decidirColdCall3bet(jugadores[id].toRank(), posicion0, posicion1)){
					jugadores[id].menos(3*BB);
					accion=Accion.CALL;
				}
			}else{
				if (bot.decidirCall3bet(jugadores[id].toRank(), posicion0, posicion1)){
					int n;
					if (posicion1==Posicion.BB)
						n=(2*BB);
					else
						n=(2*BB+1);
					jugadores[id].menos(n);
				}
			}
		}
		if (accion==null) 			
			accion=Accion.FOLD;
		return accion;
	}
	
	public ArrayList<Integer> getIdGanadores(){
		ArrayList<Integer> listaDeGanadores = new ArrayList<Integer>();
		mejorBaza(3);
		int a =0;
		if (!jugadores[a].estaJugando())a++;
		if (!jugadores[a].estaJugando())a++;
		if (bazas.size()>a)listaDeGanadores.add(bazas.get(a).getId());
		boolean finEmpates = false;
		for (int i=1; i<bazas.size() && !finEmpates;i++){
			/*if (bazas.get(i).equals(bazas.get(0)) != bazas.get(0).equals(bazas.get(i)))
				System.out.println("equalsERROR");*/
			if (bazas.get(i).equals(bazas.get(0)))
				listaDeGanadores.add(bazas.get(i).getId());
			else
				finEmpates=true;
		}
		return listaDeGanadores;
	}
	
	private void ordenar(){		
		ArrayList<Baza> b = new ArrayList<Baza>();
		Baza[] a = new Baza[bazas.size()];
		bazas.toArray(a);
		for(int x=0;x<a.length;x++){
			for(int y=x+1;y<a.length;y++){
				if(a[x]!=a[x].noMenor(a[y])){
					Baza tmp=a[x];
					a[x]=a[y];
					a[y]=tmp;
				}
				//if (x==0) empate|=a[x].isEmpate()||a[y].isEmpate();
			}
		}
		for(int x=0;x<a.length;x++){
			b.add(a[x]);
		}
		bazas=b;
	}
	
	public Mesa getMesa() {
		return mesa;
	}
	public void mejorBaza(int modoEjecucion) {
		bazas = new ArrayList<Baza>();
		if (modoEjecucion == 1) {			
			bazas.add(jugadores[0].mejorBaza(mesa));
		} else if (modoEjecucion == 2) {
			bazas.add(jugadores[0].mejorBaza(mesa));
		} else if (modoEjecucion == 3) {
			for (int i = 0; i < nJugadores; i++) {
				bazas.add(jugadores[i].mejorBaza(mesa));
			}
			ordenar();
		}
	}

	public int getNGanadores(){
		return nGanadores;
	}
	
	public void setCartas(Mesa jesa) {
		mesa=jesa;
		
	}
	
	public Mano[] getJugadores(){
		return jugadores;
	}
}
	//INUTIL
	/*
	public boolean ganador(){
		mejorBaza(3);
		ganadores=new int[nJugadores];
		nGanadores=1;
		ganadores[0]=bazas.get(0).getId();
		for(int i=1;i<bazas.size();i++){
			if (bazas.get(i).equals(bazas.get(i-1))) {
				ganadores[i]=bazas.get(i).getId();
				nGanadores++;
			} else {
				i=bazas.size();
			}
		}
		return nGanadores==1;
	}
	
	public ArrayList<Integer> ganadores(){
		ArrayList<Integer> a = null;
		ArrayList<Baza> b=new ArrayList<Baza>();
		for (int i = 0; i < nJugadores; i++) {
			bazas.add(jugadores[i].mejorBaza(mesa));
		}
		boolean lleno =false;
		TipoBaza t=null;
		while (!lleno){
			int s=TipoBaza.values().length-1;
			for(Baza j: bazas){
				if (j.getTipo().ordinal()==s){
					lleno =true;
					b.add(j);
					t=TipoBaza.values()[s];
				}
			}
		}
//		int[] s = new int[5];
//		int c1=-1;
//		int c2=-1;
//		for (Baza ba:b){
//			switch (ba.getTipo()){
//			case escaleraDeColor:
//				if (c1<ba.getMano()[1].getValor().ordinal()){
//					a=new ArrayList<Integer>();
//					a.add(ba.getId());
//					c1=ba.getMano()[1].getValor().ordinal();
//				}else if (c1==ba.getMano()[1].getValor().ordinal()) a.add(ba.getId());
//				break;
//			case poker:
//				if (c1<ba.getMano()[1].getValor().ordinal()){
//					a=new ArrayList<Integer>();
//					a.add(ba.getId());
//					c1=ba.getMano()[1].getValor().ordinal();
//					c2=ba.getMano()[4].getValor().ordinal();
//				}//else if (c1==ba.getMano()[1].getValor().ordinal() && c2<ba.getMano()[4].getValor().ordinal())
//			
//			}
//			
//		}
		return a;
 	}
	

*/


