package hja.model;

public class Baza{
	private TipoBaza tipo;
	private Carta[] mano;
	private boolean[] draws = new boolean[5];//escalera, color y escalera de color y gutshot
	private int id;
	private boolean empate;
	
	public Baza(TipoBaza a, Carta[] c, int id){
		tipo=a;
		mano=c;
		this.id = id;

	}
	
	public Baza(TipoBaza a, Carta[] c, boolean escalera, boolean color,
			boolean escaleraDeColor, boolean gutShot, boolean gutShotColor, int id){
		tipo=a;
		mano=c;
		draws[0]=escalera;
		draws[1]=color;
		draws[2]=escaleraDeColor;
		draws[3]=gutShot;
		draws[4]=gutShotColor;
		this.id = id;
		
	}
	
	public Baza noMenor(Baza otra){
		empate=false;
		if (tipo.ordinal()>otra.getTipo().ordinal())
			return this;
		else if(tipo.ordinal()<otra.getTipo().ordinal())
			return otra;
		else{
			switch (tipo){
			case escaleraDeColor:	if((mano[1].getValor().ordinal()==otra.getMano()[1].getValor().ordinal())) empate=true;
									if(mano[1].getValor().ordinal()>=otra.getMano()[1].getValor().ordinal()) return this;
									else return otra;
			
			case poker:				if(mano[0].getValor().ordinal()>otra.getMano()[0].getValor().ordinal()
										|| (mano[0].getValor()==Valor.A && otra.getMano()[0].getValor()!=Valor.A))
										return this;
									if(mano[0].getValor().ordinal()==otra.getMano()[0].getValor().ordinal() &&
											mano[4].getValor().ordinal()==otra.getMano()[4].getValor().ordinal()) 
										empate=true;
									if(mano[0].getValor().ordinal()==otra.getMano()[0].getValor().ordinal() &&
											((mano[4].getValor().ordinal()>=otra.getMano()[4].getValor().ordinal() &&
											(otra.getMano()[4].getValor()!=Valor.A)) || mano[4].getValor()==Valor.A)) 
										return this;
									else
										return otra;
			
			case fullHouse:			if(mano[0].getValor().ordinal()==otra.getMano()[0].getValor().ordinal() &&
									mano[4].getValor().ordinal()==otra.getMano()[4].getValor().ordinal()) 
										empate=true;
									if(mano[0].getValor().ordinal()>otra.getMano()[0].getValor().ordinal()
										|| (mano[0].getValor()==Valor.A && otra.getMano()[0].getValor()!=Valor.A))
										return this;
									else if(mano[0].getValor().ordinal()==otra.getMano()[0].getValor().ordinal() &&
											mano[4].getValor().ordinal()>=otra.getMano()[4].getValor().ordinal())
										return this;
									else
										return otra;
			
			case color:				if (mano[0].getValor()==Valor.A && otra.getMano()[0].getValor()!=Valor.A)
										return this;
									if (mano[0].getValor()!=Valor.A && otra.getMano()[0].getValor()==Valor.A)
										return otra;
									for(int x=0;x<5;x++){
										if(mano[x].getValor().ordinal()>otra.getMano()[x].getValor().ordinal()) return this;
										else if (mano[x].getValor().ordinal()<otra.getMano()[x].getValor().ordinal()) return otra;
									}
									empate=true;
									return this;
			
			case escalera:			if(mano[1].getValor().ordinal()==otra.getMano()[1].getValor().ordinal()) empate = true;
									if(mano[1].getValor().ordinal()>=otra.getMano()[1].getValor().ordinal()) return this;
									else return otra;
			
			case trio:				if(mano[0].getValor().ordinal()>otra.getMano()[0].getValor().ordinal()
										|| (mano[0].getValor()==Valor.A && otra.getMano()[0].getValor()!=Valor.A))
										return this;
									else if((mano[0].getValor().ordinal()<otra.getMano()[0].getValor().ordinal()
											|| mano[0].getValor()!=Valor.A) && otra.getMano()[0].getValor()==Valor.A)
										return otra;
									else{
										for(int x=3;x<5;x++){
											if(mano[x].getValor().ordinal()>otra.getMano()[x].getValor().ordinal()
													|| (mano[x].getValor()==Valor.A && otra.getMano()[x].getValor()!=Valor.A)) return this;
											else if (mano[x].getValor().ordinal()<otra.getMano()[x].getValor().ordinal()
													|| (mano[x].getValor()!=Valor.A && otra.getMano()[x].getValor()==Valor.A)) return otra;
										}
										empate=true;
										return this;
									}
			
			case doblesParejas:		if(mano[0].getValor().ordinal()>otra.getMano()[0].getValor().ordinal()
										|| (mano[0].getValor()==Valor.A && otra.getMano()[0].getValor()!=Valor.A))
										return this;
									else if(mano[0].getValor().ordinal()==otra.getMano()[0].getValor().ordinal() &&
											mano[3].getValor().ordinal()>otra.getMano()[3].getValor().ordinal())
										return this;
									else if ((mano[4].getValor().ordinal()>otra.getMano()[4].getValor().ordinal()
											&& otra.getMano()[4].getValor()!=Valor.A) || mano[0].getValor()==Valor.A)
										return this;
									else{
										empate=true;
										return otra;
									}
			
			case pareja:			if(mano[0].getValor().ordinal()>otra.getMano()[0].getValor().ordinal()
										|| (mano[0].getValor()==Valor.A && otra.getMano()[0].getValor()!=Valor.A))
										return this;
									else if(mano[0].getValor().ordinal()<otra.getMano()[0].getValor().ordinal()
											|| (mano[0].getValor()!=Valor.A) && otra.getMano()[0].getValor()==Valor.A)
										return otra;
									for(int x=2;x<5;x++){
										if(mano[x].getValor().ordinal()>otra.getMano()[x].getValor().ordinal()
												|| (mano[x].getValor()==Valor.A && otra.getMano()[x].getValor()!=Valor.A)) return this;
										else if ((mano[x].getValor().ordinal()<otra.getMano()[x].getValor().ordinal()
												|| mano[x].getValor()!=Valor.A) && otra.getMano()[x].getValor()==Valor.A) return otra;
									}
									empate=true;
									return this;
			
			case cartaAlta:			for(int x=0;x<5;x++){
										if(mano[x].getValor().ordinal()>otra.getMano()[x].getValor().ordinal()
												|| (mano[x].getValor()==Valor.A && otra.getMano()[x].getValor()!=Valor.A)) return this;
										else if ((mano[x].getValor().ordinal()<otra.getMano()[x].getValor().ordinal()
												|| mano[x].getValor()!=Valor.A) && otra.getMano()[x].getValor()==Valor.A) return otra;
									}
									empate=true;
									return this;
			
			default:				return this;
			}
		}
	}
	
	public String toString(int modoEjecucion){
		StringBuilder cadena = new StringBuilder(new String(""));
		if(modoEjecucion==3) cadena.append(id+": ");
		for(Carta c: mano)
			cadena.append(c.toString());
		cadena.append(System.lineSeparator());
		cadena.append(" -Best Hand: "+ tipo.toString());
		cadena.append(System.lineSeparator());
		if (modoEjecucion!=3){
			String j=" -Draw: ";
			if(draws[0]){
				cadena.append(j+"Straigt");
				if(draws[3])cadena.append(" Gutshot");
				cadena.append(System.lineSeparator());
			}
			if(draws[1])
				cadena.append(j+"Flush"+System.lineSeparator());
			if(draws[2]){
				cadena.append(j+"Flush Straigt");
				if(draws[4])cadena.append(" Gutshot");
				cadena.append(System.lineSeparator());
			}
		}
		return cadena.toString();
	}
	
	public int getId(){
		return id;
	}
	public TipoBaza getTipo() {
		return tipo;
	}
	public Carta[] getMano() {
		return mano;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Baza)) {
			return false;
		}
		Baza other = (Baza) obj;
		if (tipo!=other.tipo)
			return false;
		for(int i=0; i<5;i++){
			if (mano[i].getValor()!=other.mano[i].getValor())
				return false;
		}
		/*
		for(Carta carta: mano) {
			boolean encontradaIgual=false;
			for (Carta carta2: other.mano){
				if (carta.getValor()==carta2.getValor()) {
					encontradaIgual=true;
					carta2=null;
				}
			}
			if (!encontradaIgual) return false;
		}/*
		for(Carta carta: other.mano) {
			boolean encontradaIgual=false;
			for (Carta carta2: mano){
				if (carta.getValor()==carta2.getValor()) {
					encontradaIgual=true;
				}
			}
			if (!encontradaIgual) return false;
		}
		*/
		return true;
	}

	public boolean isEmpate() {
		return empate;
	}

	
	
	
	/*
	public boolean equals(Object z){
		boolean b = (z instanceof Baza);
		for(int i=0;i<5;i++){
			int j=0;
			boolean encontrada=true;
			while(!encontrada && j<5){
				if (mano[i].equals(((Baza) z).getMano()[j]))
					encontrada=true;
				else
					j++;
			}
			if (j==5) b=false;
		}
		return b;
	}*/
}