package hja.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class Comparador {

	private final int MANOS = 50;
	private int nJugadores = 0;
	private HashSet<Carta> burned = new HashSet<Carta>();
	private HashSet<Carta> board = new HashSet<Carta>();
	private double[] victorias =new double[10];
	private int totalPruebas = 0;
	private HashSet<Carta> enUso = new HashSet<Carta>();
	private Carta[] mesaActual= new Carta[5];
	private Jugada partidaActual;

	public Comparador(List<Carta> quemada, List<Carta> mesa) {
		burned.addAll(quemada);
		board.addAll(mesa);
		enUso.addAll(quemada);
		enUso.addAll(mesa);
	}
	
	

	public float[] compara(boolean[][][] rangos,Mano[][] rank){
		nJugadores=rangos.length;
		float[] ret=  new float[nJugadores];
		enUso = new HashSet<Carta>();
		for(int is =0;is<MANOS;is++){
			int mesasPorManos=0;
			HashSet<Mesa> mesasUsadas = new HashSet<Mesa>();
			for (int i=0;i<10;i++)
				victorias[i]=0.0;
			Mano[] manosJugadores = obtenerManosDeRangos(rangos, rank);
			mesaActual = rellenarMesaPrestablecida();
			partidaActual = new Jugada(manosJugadores,nJugadores,null);
			while(mesasPorManos<80000){
				rellenarMesaAleatoria();
				mesasPorManos++;
				mesasUsadas.add(new Mesa(mesaActual));
				partidaActual.setCartas(new Mesa(mesaActual));
				evaluarGanadores();
				for(int i=0;i<mesaActual.length;i++)
					enUso.remove(mesaActual[i]);
			}
			
			for(int w=0;w<nJugadores;w++){
				enUso.remove(manosJugadores[w].getUno());
				enUso.remove(manosJugadores[w].getDos());
			}
			totalPruebas+=mesasPorManos;		
			for(int i=0;i<nJugadores;i++)
				ret[i]+=(double)victorias[i]/(mesasPorManos*MANOS);
		}
		return ret;
	}
	
	private void evaluarGanadores(){
		ArrayList<Integer> ganadores = partidaActual.getIdGanadores();
		double temp = (double)1/(double)ganadores.size();
		for(int id : ganadores)
			victorias[id]=victorias[id]+temp;
	}

	private void rellenarMesaAleatoria(){
		Random n = new Random();
		int index=5-board.size();
		while(index>0){
			boolean cartaInsertada=false;
			while (!cartaInsertada){
				Carta c = new Carta(Palo.values()[n.nextInt(4)],Valor.values()[n.nextInt(13)]);
				if(enUso.add(c)){
					cartaInsertada=true;
					mesaActual[index-1]=c;
					index--;
				}
			}
		}
	}
	
	public int getPruebas() {
		return totalPruebas;
	}

	
	private Carta[] rellenarMesaPrestablecida(){
		Carta[] mesa = new Carta[5];
		int i=5;
		if (!board.isEmpty()){
			Iterator<Carta> b = board.iterator();
			while (b.hasNext()){
				mesa[i]=b.next();
				i--;
			}
		}
		return mesa;
	}
	
	private Mano[] obtenerManosDeRangos(boolean[][][] rangos, Mano[][] rank) {
		
		Random n = new Random();
		Mano[] m = new Mano[4];
		int i = 0;
		for (boolean[][] rango : rangos) {
			int x = n.nextInt(13);
			int y = n.nextInt(13);
			while (!rango[x][y]) {
				if (++x > 12) {
					x = 0;
					if (++y > 12)
						y = 0;
				}
			}
			m[i] = autoMano(x, y, i);
			while (enUso.contains(m[i].getDos()) || enUso.contains(m[i].getUno())) {
				m[i] = autoMano(x, y, i);
			}
			enUso.add(m[i].getDos());
			enUso.add(m[i].getUno());
			i++;
		}
		return m;
	}

	private Mano autoMano(int n, int m, int p) {
		Valor v;
		Random s = new Random();
		if (n == 0)
			v = Valor.A;
		else
			v = Valor.values()[13 - n];
		Valor w;
		if (m == 0)
			w = Valor.A;
		else
			w = Valor.values()[13 - m];
		if (n >= m) {
			int a = s.nextInt(4);
			int b = s.nextInt(4);
			while (a == b) {
				a = s.nextInt(4);
				b = s.nextInt(4);
			}
			return new Mano(new Carta(Palo.values()[a], v), new Carta(Palo.values()[b], w), p);
		} else {
			int a = s.nextInt(4);
			return new Mano(new Carta(Palo.values()[a], v), new Carta(Palo.values()[a], w), p);
		}
	}

	
public Jugada jugadaAleatoria(){
	boolean[][][] rangos = new boolean[4][13][13];
	for (boolean[][] r : rangos)
		for (boolean[] a : r)
			Arrays.fill(a, true);
	
	Mano[] manosJugadores = obtenerManosDeRangos(rangos, new Mano[4][20]);
	partidaActual = new Jugada(manosJugadores,nJugadores,null);
	rellenarMesaAleatoria();
	partidaActual.setCartas(new Mesa(mesaActual));;
	return partidaActual;
	}
}
