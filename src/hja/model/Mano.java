package hja.model;
public class Mano {
	private Carta uno;
	private Carta dos;
	private Baza mejorBaza;
	private Mesa mesa;
	private int id;
	private int dinero=100;
	private boolean activo=true;
	
	public boolean estaJugando(){
		return activo;
	}
	
	public void abandona(){
		activo=false;
	}
	
	public void mas(int x){
		dinero+=x;
	}
	
	public void menos(int x){
		dinero-=x;
	}
	public int getDinero() {
		return dinero;
	}
	public Mano(Carta uno, Carta dos, int id) {
		this.uno = uno;
		this.dos = dos;
		this.id = id;
		
	}
	
	public String toRank(){
		String s="";
		if(uno.getValor().ordinal()>dos.getValor().ordinal())
			s+=uno.getValor().toString()+dos.getValor().toString();
		else if(uno.getValor().ordinal()<dos.getValor().ordinal())
			s+=dos.getValor().toString()+uno.getValor().toString();
		else return dos.getValor().toString()+dos.getValor().toString();
		if (uno.getPalo()==dos.getPalo()) return s+"s";
		else return s+"o";
	}
	
	public Baza getMejorBaza() {
		return mejorBaza;
	}
	public void setMesa(Mesa mesa) {
		this.mesa = mesa;
	}
	
	public int hashCode(){
		return uno.hashCode()+dos.hashCode();
	}
	
	public boolean equals(Object o){
		if((o instanceof Mano) && (((((Mano)o).getUno()==uno) && (((Mano)o).getDos()==dos)) || ((((Mano)o).getUno()==dos) && (((Mano)o).getDos()==uno))))
			return true;
		else
			return false;
	}
	/*
	@Override
	public void run() {
		mejorBaza = mejorBaza(mesa);
	}
	*/
	
	
	
	/**funcion que devuelve la mejor combinacion de la mano dada una mesa de 3 a 5 cartas*/
	public Baza mejorBaza(Mesa m){
		boolean[][] cartas = new boolean[4][13];
		int x=uno.getPalo().ordinal();
		int y=uno.getValor().ordinal();
		cartas[x][y]=true;
		x=dos.getPalo().ordinal();
		y=dos.getValor().ordinal();
		cartas[x][y]=true;
		for(Carta c :m.getCartas()){
			x=c.getPalo().ordinal(); //Creamo una matriz booleana de 4*13 que relleno con las cartras que tengo
			y=c.getValor().ordinal();
			cartas[x][y]=true;
		}
		int es  =0;
		int par1=0;
		int par2=0;
		Valor val1=null;
		Valor val2=null;
		boolean hayAs =false;
		boolean escalera=false;
		Carta[] comb = new Carta[5];
		boolean drawColor=false;
		boolean color=false;
		int c=0;
		int[] colo = new int[4];
		int hueco=0;
		boolean gutColor=false;
		boolean gut=false;
		boolean drawEscalera=false;
		boolean drawEscaleraColor=false;
		for(x=0;x<13;x++){							//Analizamos la matriz en busca de jugadas, draws 
			int actual=0;							//y posisbles jugadas o draws de escaleras de color
			if(cartas[0][x] || cartas[1][x] || cartas[2][x] || cartas[3][x]){
				if (x==0) hayAs=true;;
				es++;
				hueco++;
			}
			else{
				if (hueco!=es) hueco=0;
				else hueco++;
				es=0;
			}
			if (hueco==5 || es==4 || (hueco==4 && x==12 && hayAs))
				drawEscalera=true;
				if(hueco!=es)
					gut=true;
			for(int i=0;i<4;i++){
				if (cartas[i][x]){
					actual++;
					colo[i]++;
					if (colo[i]==4){
						drawColor=true;
						c=i;
					}
					if (colo[i]==5)
						color=true;
				}
			}
			if (actual>1){
				if (par1==0){
					par1=actual;
					val1=Valor.values()[x];
				}else if (par2==0){
					par2=actual;
					val2=Valor.values()[x];
				}else{
					if(!(val1==Valor.A)){
						if(par1<=actual){
							par1=actual;
							val1=Valor.values()[x];	
						}else{
							par2=actual;
							val2=Valor.values()[x];
						}
					}else if(par2<=actual){
						par2=actual;
						val2=Valor.values()[x];
					}
				}
			}
			if (es>=5 || (es>=4 && x==12 && hayAs))
				escalera=true;
		}
		if ((escalera && color) || (drawEscalera && drawColor)){//comprobamos si hay escaleras de color o si hay draws de escaleras de color
			int actual=0;										//si la hay lanzamos como mejor jugada
			hueco=0;
			for(x=0;x<13;x++){
				if(cartas[c][x]){
					actual++;
					hueco++;
				}
				else{
					if (hueco==actual)
						hueco++;
					else
						hueco=0;
					actual=0;
				}
				if (hueco==5 || actual==4 || (hueco==4 && x==12 && hayAs)){
					drawEscaleraColor=true;
					if(hueco!=actual)
						gutColor=true;
				}
				if (actual>=5){
					int il=4;
					for(y=x-4;y<=x;y++){
						comb[il]=new Carta(Palo.values()[c],Valor.values()[y]);
						il--;
					}
					return new Baza(TipoBaza.escaleraDeColor,comb, id);
				}if (actual>=4 && cartas[0][c] && x==12){
					int ill=4;
					for(y=x-3;y<=x;y++){
						comb[ill]=new Carta(Palo.values()[c],Valor.values()[y]);
						ill--;
					}
					comb[0]=new Carta(Palo.values()[c],Valor.values()[0]);
					return new Baza(TipoBaza.escaleraDeColor,comb, id);
				}
			}
		}
		if(par1==4 || par2==4){//�hay poker?
			int v;
			if (par1==4){
				for(x=0;x<4;x++)
					comb[x]=new Carta(Palo.values()[x],val1);
				v=val1.ordinal();
			}else{
				for(x=0;x<4;x++)
					comb[x]=new Carta(Palo.values()[x],val2);
				v=val2.ordinal();
			}
			if (v!=0 && (cartas[0][0] || cartas[1][0] || cartas[2][0] || cartas[3][0])){
				if(cartas[0][0]) comb[4]= new Carta(Palo.values()[0],Valor.values()[0]);
				else if(cartas[1][0]) comb[4]= new Carta(Palo.values()[1],Valor.values()[0]);
				else if(cartas[2][0]) comb[4]= new Carta(Palo.values()[2],Valor.values()[0]);
				else comb[4]= new Carta(Palo.values()[3],Valor.values()[0]);
			}else{
				for(x=0;x<13;x++){
					if (v!=x){
						if(cartas[0][x])
							comb[4]= new Carta(Palo.values()[0],Valor.values()[x]);
						else if(cartas[1][x])	 
							comb[4]= new Carta(Palo.values()[1],Valor.values()[x]);
						else if(cartas[2][x])	 
							comb[4]= new Carta(Palo.values()[2],Valor.values()[x]);
						else if(cartas[3][x])					
							comb[4]= new Carta(Palo.values()[3],Valor.values()[x]);
					}
				}
			}
			return new Baza(TipoBaza.poker,comb, id);//lanzamos poker
		}
		if((par1>2 && par2>=2) ||(par1>=2) && (par2>2)){//�hay full?
			
			if ((par1==3 && par2==2) || (par1==3 && ((val1.ordinal()>=val2.ordinal()) || val1== Valor.A))){
				for(x=0,y=0;y<3;x++){
					if (cartas[x][val1.ordinal()]){
						comb[y]=new Carta(Palo.values()[x], val1);
						y++;
					}
				}
				for(x=0;y<5;x++){
					if (cartas[x][val2.ordinal()]){
						comb[y]=new Carta(Palo.values()[x], val2);
						y++;
					}
				}
			}else{
				for(x=0,y=0;y<3;x++){
					if (cartas[x][val2.ordinal()]){
						comb[y]=new Carta(Palo.values()[x], val2);
						y++;
					}
				}
				for(x=0;y<5;x++){
					if (cartas[x][val1.ordinal()]){
						comb[y]=new Carta(Palo.values()[x], val1);
						y++;
					}
				}
			}
			return new Baza(TipoBaza.fullHouse,comb, id);//lanzamos full
		}
		if (color){//�hay color?
			x=0;
			if(cartas[c][0]){
				comb[x]=new Carta(Palo.values()[c],Valor.A);
				x++;
			}for(y=12;x<5;y--){
				if (cartas[c][y]){
					comb[x]=new Carta(Palo.values()[c],Valor.values()[y]);
					x++;
				}
			}
			return new Baza(TipoBaza.color,comb,false,false,drawEscaleraColor,false,gutColor, id);//lanzamos color
		}
		if (escalera){//�hay escalera?/*
			if(hayAs){
				if(cartas[0][0])
					comb[0]= new Carta(Palo.values()[0],Valor.values()[0]);
				else if(cartas[1][0])
					comb[0]= new Carta(Palo.values()[1],Valor.values()[0]);
				else if(cartas[2][0])
					comb[0]= new Carta(Palo.values()[2],Valor.values()[0]);
				else if(cartas[3][0])
					comb[0]= new Carta(Palo.values()[3],Valor.values()[0]);
				x=1;
			}else
				x=0;
			for(y=12;x<5;y--){
				if(cartas[0][y]){
					comb[x]= new Carta(Palo.values()[0],Valor.values()[y]);
					x++;
				}
				else if(cartas[1][y]){
					comb[x]= new Carta(Palo.values()[1],Valor.values()[y]);
					x++;
				}
				else if(cartas[2][y]){
					comb[x]= new Carta(Palo.values()[2],Valor.values()[y]);
					x++;
				}
				else if(cartas[3][y]){
					comb[x]= new Carta(Palo.values()[3],Valor.values()[y]);
					x++;
				}
				else x=0;
			}
			return new Baza(TipoBaza.escalera, comb,false,drawColor,drawEscaleraColor,false,gutColor, id);//lanzamos escalera
		}
		if (par1==3 || par2==3){//�hay trio?
			if (par1==3){
				for(x=0,y=0;y<3;x++){
					if (cartas[x][val1.ordinal()]){
						comb[y]=new Carta(Palo.values()[x], val1);
						y++;
					}
				}
				if (val1.ordinal()!=0){//�hay algun as como kicker?
					if(cartas[0][0]) comb[3]= new Carta(Palo.values()[0],Valor.values()[0]);
					else if(cartas[1][0]) comb[3]= new Carta(Palo.values()[1],Valor.values()[0]);
					else if(cartas[2][0]) comb[3]= new Carta(Palo.values()[2],Valor.values()[0]);
					else comb[3]= new Carta(Palo.values()[3],Valor.values()[0]);
					y++;
				}
				for(x=12;y<5;x--){//resto de kickers
					if (val1.ordinal()!=x){
						if(cartas[0][x]){ comb[y]= new Carta(Palo.values()[0],Valor.values()[y]);y++;}
						else if(cartas[1][x]){ comb[y]= new Carta(Palo.values()[1],Valor.values()[y]);y++;}
						else if(cartas[2][x]){ comb[y]= new Carta(Palo.values()[2],Valor.values()[y]);y++;}
						else if( cartas[3][x]){comb[y]= new Carta(Palo.values()[3],Valor.values()[y]);y++;}
					}
				}
			}else{
				for(x=0;y<3;x++){
					if (cartas[x][val1.ordinal()]){
						comb[y]=new Carta(Palo.values()[x], val2);
						x++;
					}
				}
				if (val2.ordinal()!=0){
					if(cartas[0][0]){ comb[3]= new Carta(Palo.values()[0],Valor.values()[0]);y++;}
					else if(cartas[1][0]){ comb[3]= new Carta(Palo.values()[1],Valor.values()[0]);y++;}
					else if(cartas[2][0]){ comb[3]= new Carta(Palo.values()[2],Valor.values()[0]);y++;}
					else if(cartas[3][0]){ comb[3]= new Carta(Palo.values()[3],Valor.values()[0]);y++;}
				}
				for(x=13;y<5;x--){
					if (val2.ordinal()!=x){
						if(cartas[0][x]){ comb[y]= new Carta(Palo.values()[0],Valor.values()[y]);y++;}
						else if(cartas[1][x]){ comb[y]= new Carta(Palo.values()[1],Valor.values()[y]);y++;}
						else if(cartas[2][x]){ comb[y]= new Carta(Palo.values()[2],Valor.values()[y]);y++;}
						else if(cartas[3][x]){comb[y]= new Carta(Palo.values()[3],Valor.values()[y]);y++;}
					}
				}
			}
			return new Baza(TipoBaza.trio,comb,drawEscalera,drawColor,drawEscaleraColor,gut,gutColor, id);//lanzamos trio
		}
		if (par1==2){ 
			if (par2<2 || ((val1.ordinal()>=val2.ordinal() && val2!=Valor.A) || val1==Valor.A)){
				for(x=0,y=0;y<2;x++){
					if (cartas[x][val1.ordinal()]){
						comb[y]=new Carta(Palo.values()[x], val1);
						y++;
					}
				}
				if (par2==2){//�hay dobles parejas?
					for(x=0;y<4;x++){
						if (cartas[x][val2.ordinal()]){
							comb[y]=new Carta(Palo.values()[x], val2);
							y++;
						}
					}
				}
			}else if (par2==2 && (val1.ordinal()<val2.ordinal() || val2==Valor.A)){
				for(x=0,y=0;y<2;x++){
					if (cartas[x][val2.ordinal()]){
						comb[y]=new Carta(Palo.values()[x], val2);
						y++;
					}
				}
				for(x=0;y<4;x++){
					if (cartas[x][val1.ordinal()]){
						comb[y]=new Carta(Palo.values()[x], val1);
						y++;
					}
				}
			}
			if (val1.ordinal()!=0 && (par2==0 || val2.ordinal()!=0)){
				if(cartas[0][0]) {comb[y]= new Carta(Palo.values()[0],Valor.values()[0]);y++;}
				else if(cartas[1][0]){ comb[y]= new Carta(Palo.values()[1],Valor.values()[0]);y++;}
				else if(cartas[2][0]){ comb[y]= new Carta(Palo.values()[2],Valor.values()[0]);y++;}
				else if(cartas[3][0]){comb[y]= new Carta(Palo.values()[3],Valor.values()[0]);y++;}
			}
			for(x=12;y<5;x--){
				if (val1.ordinal()!=x&& (par2==0 || val2.ordinal()!=x)){
					if(cartas[0][x]) {comb[y]= new Carta(Palo.values()[0],Valor.values()[x]);y++;}
					else if(cartas[1][x]){ comb[y]= new Carta(Palo.values()[1],Valor.values()[x]);y++;}
					else if(cartas[2][x]){ comb[y]= new Carta(Palo.values()[2],Valor.values()[x]);y++;}
					else if(cartas[3][x]){ comb[y]= new Carta(Palo.values()[3],Valor.values()[x]);y++;}
				}
			}
			if (par2==2){
				return new Baza(TipoBaza.doblesParejas,comb,drawEscalera,drawColor,drawEscaleraColor,gut,gutColor, id);//lanzamos DoubleTwins
			}
			return new Baza(TipoBaza.pareja,comb,drawEscalera,drawColor,drawEscaleraColor,gut,gutColor, id);//lanzamos pareja
		}
		y=0;
		if (hayAs){
			if(cartas[0][0]) {comb[0]= new Carta(Palo.values()[0],Valor.values()[0]);y++;}
			else if(cartas[1][0]){ comb[0]= new Carta(Palo.values()[1],Valor.values()[0]);y++;}
			else if(cartas[2][0]){ comb[0]= new Carta(Palo.values()[2],Valor.values()[0]);y++;}
			else if(cartas[3][0]){comb[0]= new Carta(Palo.values()[3],Valor.values()[0]);y++;}
		}
		for(x=12;y<5;x--){
			if(cartas[0][x]) {comb[y]= new Carta(Palo.values()[0],Valor.values()[x]);y++;}
			else if(cartas[1][x]){ comb[y]= new Carta(Palo.values()[1],Valor.values()[x]);y++;}
			else if(cartas[2][x]){ comb[y]= new Carta(Palo.values()[2],Valor.values()[x]);y++;}
			else if(cartas[3][x]){ comb[y]= new Carta(Palo.values()[3],Valor.values()[x]);y++;}
			
		}//�hay alguien ahi?
		return new Baza(TipoBaza.cartaAlta,comb,drawEscalera,drawColor,drawEscaleraColor,gut,gutColor, id);
		//lanzamos que perderemos esta mano si no se aparece la virgen Maria
	}

	
	public Carta getUno() {
		return uno;
	}
	public void setUno(Carta uno) {
		this.uno = uno;
	}
	public Carta getDos() {
		return dos;
	}
	public void setDos(Carta dos) {
		this.dos = dos;
	}
	public String toString() {
		if (uno.getPalo().ordinal()<=dos.getPalo().ordinal())
		return uno.toString()+dos.toString();
		else return dos.toString() + uno.toString();
	}
}
	/*				FUTURO				CALCULO EN PREFLOP
	public void calculoPreflopPoker(){
		if (iguales)
			poker = Constantes.probPrePoker2;
		else
			poker = Constantes.probPrePoker1*2;
	}

	public void calculoPreflopEscaleraColor(){
		//escalera de color
		if (iguales || !near){
			for(Carta c = uno; c!=null; ){
				if (c.getValor().ordinal()>3 || c.getValor().ordinal()<10){
					escaleraColor+= Constantes.probPreEsCo1*5;
				}
				if (c.getValor().ordinal()==3 || c.getValor().ordinal()==10){
					escaleraColor+= Constantes.probPreEsCo1*4;
				}
				if (c.getValor().ordinal()==2 || c.getValor().ordinal()==11){
					escaleraColor+= Constantes.probPreEsCo1*3;
				}
				if (c.getValor().ordinal()>2 || c.getValor().ordinal()<11){
					escaleraColor+= Constantes.probPreEsCo1*2;
				}
				if (iguales || c==dos ) c= null;
				else c=dos;
			}
		}else if(near){
			if(distancia==1){
				if (menor<3)
					escaleraColor = Constantes.probPreEsCo2_1 *(menor+1);
				else if (mayor<11)
					escaleraColor = Constantes.probPreEsCo2_1 *(14-mayor);
				else
					escaleraColor = Constantes.probPreEsCo2_1 * 4;
				if (mayor<10)
					escaleraColor+= Constantes.probPreEsCo1;
			}
			else if (distancia==2){
				if (menor<2)
					escaleraColor = Constantes.probPreEsCo2_2 *(menor+1);
				else if (mayor<12)
					escaleraColor = Constantes.probPreEsCo2_2 *(14-mayor);
				else
					escaleraColor = Constantes.probPreEsCo2_2 * 3;
				if (mayor<10)
					escaleraColor+= Constantes.probPreEsCo1;
				if (menor>4)
					escaleraColor+= Constantes.probPreEsCo1;
			}
			else if (distancia==3){
				if (menor<1)
					escaleraColor = Constantes.probPreEsCo2_3 *(menor+1);
				else if (mayor<13)
					escaleraColor = Constantes.probPreEsCo2_3 *(14-mayor);
				else
					escaleraColor = Constantes.probPreEsCo2_3 * 2;
				int i = 0;
				if (menor>3)
					i++;
				if (menor>2)
					i++;
				if (mayor<10)
					i++;
				if (mayor<11)
					i++;
				escaleraColor+= Constantes.probPreEsCo1 * i;
				
				
			}else{ //distancia == 4
				escaleraColor = Constantes.probPreEsCo2_4;
				int i = 0;
				if (menor>3)
					i++;
				if (menor>2)
					i++;
				if (menor>1)
					i++;
				if (mayor<10)
					i++;
				if (mayor<11)
					i++;
				if (mayor<12)
					i++;
				escaleraColor+= Constantes.probPreEsCo1 * i;
			}
		}
	}*/
	/*   PASADO
	 * 
	 * 
	
	public boolean escalera(Carta[] ordenados){
		 boolean b= ordenados[5].getValor().ordinal()==ordenados[4].getValor().ordinal()+1
				&& ordenados[4].getValor().ordinal()==ordenados[3].getValor().ordinal()+1
				&& ordenados[3].getValor().ordinal()==ordenados[2].getValor().ordinal()+1 
				&& ordenados[2].getValor().ordinal()==ordenados[1].getValor().ordinal()+1;
		 
		 b= b && ((ordenados[5].getValor().ordinal()==2 || (ordenados[2].getValor().ordinal()==13) 
				 && ordenados[0].getValor().ordinal()==0)
				 || ordenados[1].getValor().ordinal()==ordenados[0].getValor().ordinal()+1);
		return b;
	}
	
	
	public Baza mejorBazaFlop(Mesa m){
		Carta[] ordenados = new Carta[5];
		Baza b=null;
		if (suited && dos.getPalo()==m.getCartas()[0].getPalo() && m.getCartas()[1].getPalo()==uno.getPalo()
				&& m.getCartas()[2].getPalo()==uno.getPalo()){
			int c=5;
			for(Valor v: Valor.values()){
				if (v==Valor.A){
					if (uno.getValor()==v){
						ordenados[0]=uno;						
					}else if (dos.getValor()==v)
						ordenados[0]=dos;
				}else{
					if(uno.getValor()==v) ordenados[c]=uno;
					else if(dos.getValor()==v) ordenados[c]=dos;
					else if(m.getCartas()[0].getValor()==v) ordenados[c]=m.getCartas()[0];
					else if(m.getCartas()[1].getValor()==v) ordenados[c]=m.getCartas()[1];
					else if(m.getCartas()[2].getValor()==v) ordenados[c]=m.getCartas()[2];
					c--;
				}
						
			}
			if (near && escalera(ordenados)) {
				return new Baza(TipoBaza.escaleraDeColor, ordenados);
			}else
				b= new Baza(TipoBaza.color,ordenados);
		}
		if(iguales){
			if (m.getCartas()[0].getValor()==m.getCartas()[1].getValor() ||
					m.getCartas()[0].getValor()==m.getCartas()[2].getValor() ||
					m.getCartas()[2].getValor()==m.getCartas()[1].getValor()){
				if(uno.getValor()==m.getCartas()[0].getValor()){
					ordenados[2]=m.getCartas()[0];
					if (uno.getValor()==m.getCartas()[1].getValor()){
						ordenados[3]=m.getCartas()[1];
						ordenados[4]=m.getCartas()[2];
					}else{
						ordenados[3]=m.getCartas()[2];
						ordenados[4]=m.getCartas()[1];
					}
				}else if (uno.getValor()==m.getCartas()[1].getValor()){
					ordenados[2]=m.getCartas()[1];
					ordenados[3]=m.getCartas()[2];
					ordenados[4]=m.getCartas()[0];
				}
				ordenados[0]=uno;
				ordenados[1]=dos;
				return new Baza(TipoBaza.poker,ordenados);
			}
		}else{
			//if(m.getCartas())
		}
		
		return b;
	}
	
	*/
	
	/*Atributos Antiguos
	 * 
	 * 
	/*private boolean iguales;
	private boolean suited;
	private boolean near;
	private int distancia;
	private int menor;
	private int mayor;
	private float escaleraColor;
	private float poker;
	private float fullHouse;
	private float color;
	private float escalera;
	private float trio;
	private float doblesParejas;
	private float parejas;
	private float cartaAlta;
	 */
	
	// RESTOS ANTIGUOS DE LA CONSTRUCTORA
	

	/*escaleraColor=0;
	poker=0;
	if (this.uno.getValor() == this.dos.getValor()) iguales = true;
	else iguales = false;
	if (this.uno.getPalo() == this.dos.getPalo()) suited = true;
	else suited = false;
	
	distancia = this.uno.getValor().ordinal() - this.dos.getValor().ordinal();
	if (this.uno.getValor() == Valor.A && distancia >= 4) {
		distancia = this.dos.getValor().ordinal() - 13;
	}
	if (this.dos.getValor() == Valor.A && distancia >= 4) {
		distancia = this.uno.getValor().ordinal() - 13;
	}
	distancia = Math.abs(distancia);
	if (distancia > 0 && distancia < 4) near = true;
	else near = false;
	if (!iguales){
		menor = Math.min(uno.getValor().ordinal(),dos.getValor().ordinal());
		mayor = Math.max(uno.getValor().ordinal(),dos.getValor().ordinal());
		if (uno.getValor().ordinal()==0 || dos.getValor().ordinal()==0){
			mayor=13;
		}
	}*/

