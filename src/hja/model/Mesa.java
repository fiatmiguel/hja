package hja.model;

public class Mesa {
	private Carta[] cartas;
	private int cartasEnMesa;
	
	public Mesa(Carta[] x){
		if (x.length<=5)
			cartas=x;
		cartasEnMesa=x.length;
	}
	
	public Mesa(Carta flop1, Carta flop2, Carta flop3, Carta turn, Carta river) {
		cartasEnMesa=5;
		if(turn == null)
			cartasEnMesa--;
		if(river == null)
			cartasEnMesa--;
		cartas = new Carta[cartasEnMesa];
		cartas[0] = flop1;
		cartas[1] = flop2;
		cartas[2] = flop3;
		if(turn != null)
			cartas[3]=turn;
		if(river!=null)
			cartas[4]=river;
		
	}/*
	public String toString() {
		return flop1.toString() + flop2.toString() + flop3.toString() + turn.toString();
	}*/

	public void anyadirCarta(Carta c){
		cartasEnMesa++;
		Carta[] m =new Carta[cartasEnMesa];
		for(int x=0;x<cartasEnMesa-1;x++){
			m[x]=cartas[x];
		}
		m[cartasEnMesa-1]=c;
		cartas=m;
	}
	
	public Carta[] getCartas() {
		return cartas;
	}

	public int getCartasEnMesa() {
		return cartasEnMesa;
	}
	
	public boolean equals(Object o) {
		return (o instanceof Mesa && o.hashCode()==hashCode());
	}
	
	public int hashCode(){
		int n=17;
		for(Carta c:cartas)
			n+=c.hashCode();
		return n;
	}

	public void setCartas(Carta[] cartas) {
		this.cartas = cartas;
		this.cartasEnMesa=cartas.length;
	}

	
	
}
