package hja.model;

public enum Palo {
	clover, diamonds ,heart, spade; 
	public String toString() {
		switch(this) {
			case clover: return "c"; 
			case diamonds: return "d"; 
			case heart: return "h"; 
			case spade: return "s";
		}
		return null;
	}
}
