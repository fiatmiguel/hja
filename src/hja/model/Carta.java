package hja.model;

public class Carta {
	private Palo palo;
	private Valor valor;
	public Carta(Palo palo, Valor valor) {
		this.palo = palo;
		this.valor = valor;
		
	}
	public Palo getPalo() {
		return palo;
	}
	public Valor getValor() {
		return valor;
	}
	public String toString() {
		return valor.toString() + palo.toString();
	}
	
	public int hashCode(){
		int hash = 7;
		hash=hash*67+palo.ordinal();
		hash=hash*107+valor.ordinal();
		return hash;
	}
	
	@Override
	public boolean equals(Object o){
		if (o.hashCode()!=hashCode()) return false;
		if((o instanceof Carta) && (((Carta)o).getValor().ordinal()== this.valor.ordinal()) && ((Carta)o).getPalo().ordinal()== this.palo.ordinal())
			return true;
		else
			return false;
		
	}
	public static String cardToString(Carta[] cartas) {
		Carta[] aux = new Carta[cartas.length];
		int contador = 0;
		StringBuilder s = new StringBuilder(new String(""));
		for (Palo p :Palo.values()) {
			int ini=contador;
			for (int i = 0; i < cartas.length; i++) {
				if (cartas[i].getPalo()== p) {
					aux[contador] = cartas[i];
					contador++;
				}
			}
			for (int i = ini;i<contador;i++){
				for (int j=i+1;j<contador;j++){
					if (aux[j].getValor().ordinal()<aux[i].getValor().ordinal()){
						Carta au=aux[j];
						aux[j]=aux[i];
						aux[i]=au;
					}
				}
			}
		}
		for(Carta c:aux)
			s.append(c.toString());
		return s.toString();
	}

}
