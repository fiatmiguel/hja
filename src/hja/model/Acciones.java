package hja.model;

public class Acciones {
	public static int posVsPosColdCall(Posicion posicion1, Posicion posicion2) {
		switch (posicion1) {
			case CO: {
				switch (posicion2) {
					case BTN: {
						return 14;
					}
					case SB: {
						return 6;
					}
					case BB: {
						return 10;
					}
				}
				break;
			}
			case BTN: {
				switch(posicion2) {
					case SB: {
						return 7;
					}
					case BB: {
						return 28;
					}
				}
				break;
			}
			case SB: {
				switch(posicion2) {
					case BB: {
						return 45;
					}
				}
			}
		}
		return 0;
	}
	public static int posVsPos3Bet(Posicion posicion1, Posicion posicion2) {
		switch (posicion1) {
			case CO: {
				switch (posicion2) {
					case BTN: {
						return 7;
					}
					case SB: {
						return 8;
					}
					case BB: {
						return 9;
					}
				}
				break;
			}
			case BTN: {
				switch(posicion2) {
					case SB: {
						return 18;
					}
					case BB: {
						return 17;
					}
				}
				break;
			}
			
		}
		return 0;
	}
	public static int posVsPosCall3Bet(Posicion posicion1, Posicion posicion2) {
			switch (posicion1) {
			case CO: {
				switch (posicion2) {
					case BTN: {
						return 14;
					}
					case SB: {
						return 6;
					}
					case BB: {
						return 10;
					}
				}
				break;
			}
			case BTN: {
				switch(posicion2) {
					case SB: {
						return 7;
					}
					case BB: {
						return 28;
					}
				}
				break;
			}
			case SB: {
				switch(posicion2) {
					case BB: {
						return 45;
					}
				}
			}
		}
		return 0;
	}
	public static int oR(Posicion posicion) {
		switch(posicion) {
			case CO: return 50;
			case SB: return 100;
			case BTN: return 100;
		}
		return 0;
	}
	public static boolean willMa(String par, Posicion posicion, String accion) {
		String s[] = new String[106];
		int utg = 21, mp = 29, co = 60, sm = 106, rango = 0;
		s[0] = new String("AA");
		s[1] = new String("KK");
		s[2] = new String("QQ");
		s[3] = new String("JJ");
		s[4] = new String("TT");
		s[5] = new String("99");
		s[6] = new String("88");
		s[7] = new String("77");
		s[8] = new String("66");
		s[9] = new String("AKo");
		s[10] = new String("AQo");
		s[11] = new String("AJo");
		s[12] = new String("ATo");
		s[13] = new String("AKs");
		s[14] = new String("AQs");
		s[15] = new String("AJs");
		s[16] = new String("ATs");
		s[17] = new String("A9s");
		s[18] = new String("KQo");
		s[19] = new String("KQs");
		s[20] = new String("KJs");
		s[21] = new String("55");
		s[22] = new String("A8s");
		s[23] = new String("A9o");
		s[24] = new String("KJo");
		s[25] = new String("KTs");
		s[26] = new String("QJs");
		s[27] = new String("QTs");
		s[28] = new String("JTs");
		s[29] = new String("A7s");
		s[30] = new String("A6s");
		s[31] = new String("A5s");
		s[32] = new String("A4s");
		s[33] = new String("A3s");
		s[34] = new String("A2s");
		s[35] = new String("K9s");
		s[36] = new String("K8s");
		s[37] = new String("K7s");
		s[38] = new String("K6s");
		s[39] = new String("K5s");
		s[40] = new String("Q9s");
		s[41] = new String("Q8s");
		s[42] = new String("Q7s");
		s[43] = new String("J9s");
		s[44] = new String("J8s");
		s[45] = new String("T9s");
		s[46] = new String("T8s");
		s[47] = new String("98s");
		s[48] = new String("KTo");
		s[49] = new String("QJo");
		s[50] = new String("A8o");
		s[51] = new String("A7o");
		s[52] = new String("A5o");
		s[53] = new String("K9o");
		s[54] = new String("QTo");
		s[55] = new String("Q9o");
		s[56] = new String("K9o");
		s[57] = new String("JTo");
		s[58] = new String("J9o");
		s[59] = new String("T9o");
		s[60] = new String("44");
		s[61] = new String("33");
		s[62] = new String("A6o");
		s[63] = new String("A4o");
		s[64] = new String("A3o");
		s[65] = new String("A2o");
		s[66] = new String("K8o");
		s[67] = new String("K7o");
		s[68] = new String("K6o");
		s[69] = new String("K5o");
		s[70] = new String("K4o");
		s[71] = new String("Q8o");
		s[72] = new String("Q7o");
		s[73] = new String("Q6o");
		s[74] = new String("J8o");
		s[75] = new String("J7o");
		s[76] = new String("T8o");
		s[77] = new String("T7o");
		s[78] = new String("98o");
		s[79] = new String("97o");
		s[80] = new String("87o");
		s[81] = new String("K4s");
		s[82] = new String("K3s");
		s[83] = new String("K2s");
		s[84] = new String("Q6s");
		s[85] = new String("Q5s");
		s[86] = new String("Q4s");
		s[87] = new String("Q3s");
		s[88] = new String("Q2s");
		s[89] = new String("J7s");
		s[90] = new String("J6s");
		s[91] = new String("J5s");
		s[92] = new String("J4s");
		s[93] = new String("J3s");
		s[94] = new String("T7s");
		s[95] = new String("T6s");
		s[96] = new String("T5s");
		s[97] = new String("97s");
		s[98] = new String("96s");
		s[99] = new String("95s");
		s[100] = new String("87s");
		s[101] = new String("86s");
		s[102] = new String("85s");
		s[103] = new String("76s");
		s[104] = new String("75s");
		s[105] = new String("65s");
		if(posicion == Posicion.CO) {
			rango = co;
		} else if (posicion == Posicion.SB) {
			rango = sm;
		} else if (posicion == Posicion.BTN) {
			rango = sm;
		}
		if (accion.equalsIgnoreCase("OR")) {
			for (int i = 0; i < rango; i++) {
				if (s[i].equalsIgnoreCase(par)) return true;
			}
		} else {
			for (int i = 0; i < rango; i++) {
				if (s[i].equalsIgnoreCase(par)) return false;
			}
			return true;
		}
		
		return false;
	}
	public static Posicion charToPosicion(String s) {
		if (s.equalsIgnoreCase("CO")) return Posicion.CO;
		else if (s.equalsIgnoreCase("BTN")) return Posicion.BTN;
		else if (s.equalsIgnoreCase("SB")) return Posicion.SB;
		
		return null;
		
	}
	public static boolean janda(String par, int rango) {
		String s[] = new String[91];
		s[0] = new String("AA");
		s[1] = new String("33");
		s[2] = new String("KK");
		s[3] = new String("QQ");
		s[4] = new String("JJ");
		s[5] = new String("TT");
		s[6] = new String("99");
		s[7] = new String("88");
		s[8] = new String("77");
		s[9] = new String("66");
		s[10] = new String("55");
		s[11] = new String("44");
		s[12] = new String("AKo");
		s[13] = new String("AQo");
		s[14] = new String("AJo");
		s[15] = new String("KQo");
		s[16] = new String("AKs");
		s[17] = new String("AQs");
		s[18] = new String("AJs");
		s[19] = new String("ATs");
		s[20] = new String("KQs");
		s[21] = new String("KJs");
		s[22] = new String("KTs");
		s[23] = new String("QJs");
		s[24] = new String("QTs");
		s[25] = new String("JTs");
		s[26] = new String("J9s");
		s[27] = new String("T9s");
		s[28] = new String("98s");
		s[29] = new String("87s");
		s[30] = new String("76s");
		s[31] = new String("65s");
		s[32] = new String("22");
		s[33] = new String("ATo");
		s[34] = new String("A9s");
		s[35] = new String("A8s");
		s[36] = new String("A7s");
		s[37] = new String("A5s");
		s[38] = new String("T8s");
		s[39] = new String("97s");
		s[40] = new String("86s");
		s[41] = new String("75s");
		s[42] = new String("54s");
		s[43] = new String("KJs");
		s[42] = new String("QJo");
		s[43] = new String("A6s");
		s[44] = new String("A4s");
		s[45] = new String("A3s");
		s[46] = new String("A2s");
		s[47] = new String("K9s");
		s[48] = new String("K8s");
		s[49] = new String("K7s");
		s[50] = new String("K6s");
		s[51] = new String("Q9s");
		s[52] = new String("Q8s");
		s[53] = new String("Q7s");
		s[54] = new String("J8s");
		s[55] = new String("64s");
		s[56] = new String("A9o");
		s[57] = new String("A8o");
		s[58] = new String("A7o");
		s[59] = new String("KTo");
		s[60] = new String("K9o");
		s[61] = new String("Q9o");
		s[62] = new String("JTo");
		s[63] = new String("J9o");
		s[64] = new String("T9o");
		s[65] = new String("98o");
		s[66] = new String("K5s");
		s[67] = new String("K4s");
		s[68] = new String("K3s");
		s[69] = new String("K2s");
		s[70] = new String("Q6s");
		s[71] = new String("Q5s");
		s[72] = new String("J7s");
		s[73] = new String("T7s");
		s[74] = new String("A6o");
		s[75] = new String("A5o");
		s[76] = new String("A4o");
		s[77] = new String("A3o");
		s[78] = new String("K8o");
		s[79] = new String("K7o");
		s[80] = new String("T8o");
		s[81] = new String("Q3s");
		s[82] = new String("Q2s");
		s[83] = new String("J6s");
		s[84] = new String("J5s");
		s[85] = new String("T6s");
		s[86] = new String("96s");
		s[87] = new String("85s");
		s[88] = new String("74s");
		s[89] = new String("53s");
		s[90] = new String("43s");
		
		
		int aux = 90*rango/100;
		for (int i = 0; i< aux; i++) {
			if (s[i].equalsIgnoreCase(par)) return true;
		}
		
		return false;
	}
}
