package hja.newGui;

import java.awt.Container;

import javax.swing.JFrame;

import hja.model.Carta;
import hja.model.Mano;
import hja.model.Palo;
import hja.model.Valor;

public class PanelEntero extends JFrame{
	private Container panel;
	public static void main(String[] args) {
		new PanelEntero().setVisible(true);
	}
	public PanelEntero() {
		PanelJuego f = new PanelJuego();
		panel = this.getContentPane();
		panel.add(f);
		f.mostrarCartas(new Mano(new Carta(Palo.diamonds, Valor.A), new Carta(Palo.clover, Valor.A), 1), null, null, null,0);
		this.setBounds(0, 0, 947, 624);
		
	}
}
