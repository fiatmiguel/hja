package hja.newGui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import hja.model.Carta;
import hja.model.Mano;

public class PanelJuego extends JPanel {
	private Image background;
	private static JTextField textField;
	private static JTextField textField_1;
	private static JTextField textField_2;
	private static JTextField textField_3;
	private static JButton button;
	
	private static JButton button_1;
	private static JButton btnNewButton;
	private static JButton btnNewButton_1;;
	private static JButton button_2;
	
	private static JButton button_3;
	private static JButton btnNewButton_2;
	
	private static JButton btnNewButton_3;
	private static JButton btnNewButton_4;
	
	private static JButton btnNewButton_5;
	
	private static JButton btnNewButton_6;
	
	private static JButton btnNewButton_7;
	private static JButton btnNewButton_8;
	private static boolean decision;
	private static boolean call;
	private static boolean check;
	private static boolean raise;
	private static boolean fold;
	/**
	 * Create the panel.
	 */
	public PanelJuego() {
		fold = false;
		decision = false;
		call = false;
		check = false;
		raise = false;
		btnNewButton = new JButton("");
		btnNewButton_1 = new JButton("");
		
		btnNewButton_2 = new JButton("");
		
		btnNewButton_3 = new JButton("");
		
		button = new JButton("");
		
		button_1 = new JButton("");
		
		button_2 = new JButton("");
	
		button_3 = new JButton("");
		
		JLabel lblJugador = new JLabel("Jugador");
		lblJugador.setForeground(Color.red);
		JLabel lblBot = new JLabel("Bot 1");
		lblBot.setForeground(Color.red);
		JLabel lblBot_1 = new JLabel("Bot 2 ");
		lblBot_1.setForeground(Color.red);
		JLabel lblBot_2 = new JLabel("Bot 3");
		lblBot_2.setForeground(Color.red);
		textField = new JTextField();
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		
		JButton btnCheck = new JButton("Check");
		btnCheck.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				check = true;
				decision = true;
				
			}
			
		});
		
		JButton btnFold = new JButton("Fold");
		btnFold.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				fold = true;
				decision = true;
				
			}
			
		});
		JButton btnRaise = new JButton("Raise");
		btnRaise.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				raise = true;
				decision = true;
				
			}
			
		});
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		
		btnNewButton_4 = new JButton("");
		
		btnNewButton_5 = new JButton("");
		
		btnNewButton_6 = new JButton("");
		
		btnNewButton_7 = new JButton("");
		btnNewButton_8 = new JButton("");
		
		JButton btnCall = new JButton("Call");
		btnCall.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				call = true;
				decision = true;
				
			}
			
		});
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(179)
					.addComponent(textField_3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(button_2, GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(button_3, GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(button, GroupLayout.DEFAULT_SIZE, 86, Short.MAX_VALUE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(button_1, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(btnCheck)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnFold)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnRaise)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnCall)
					.addGap(118))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(90)
					.addComponent(lblBot_2)
					.addPreferredGap(ComponentPlacement.RELATED, 664, Short.MAX_VALUE)
					.addComponent(lblBot)
					.addGap(125))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(362)
					.addComponent(lblJugador)
					.addContainerGap(530, Short.MAX_VALUE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(336)
					.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(485, Short.MAX_VALUE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(377)
					.addComponent(lblBot_1)
					.addContainerGap(527, Short.MAX_VALUE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 92, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnNewButton_1, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
							.addGap(32)
							.addComponent(btnNewButton_4, GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnNewButton_5, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(btnNewButton_6, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnNewButton_7, GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnNewButton_8, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
							.addComponent(btnNewButton_2, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnNewButton_3, GroupLayout.PREFERRED_SIZE, 92, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(50)
							.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 574, Short.MAX_VALUE)
							.addComponent(textField_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(33)))
					.addGap(48))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(btnNewButton_2, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(button_2, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
								.addComponent(button_3, GroupLayout.DEFAULT_SIZE, 119, Short.MAX_VALUE))
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(23)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(groupLayout.createSequentialGroup()
											.addGap(84)
											.addComponent(btnNewButton_3, GroupLayout.DEFAULT_SIZE, 132, Short.MAX_VALUE))
										.addGroup(groupLayout.createSequentialGroup()
											.addGap(5)
											.addComponent(lblBot_1)
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
											.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
												.addGroup(groupLayout.createSequentialGroup()
													.addGap(36)
													.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addComponent(btnNewButton_5, GroupLayout.PREFERRED_SIZE, 129, GroupLayout.PREFERRED_SIZE)
														.addComponent(btnNewButton_4, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
														.addComponent(btnNewButton_6, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)))
												.addGroup(groupLayout.createSequentialGroup()
													.addGap(35)
													.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
														.addComponent(btnNewButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
														.addComponent(btnNewButton_1, GroupLayout.DEFAULT_SIZE, 131, Short.MAX_VALUE)))))))
								.addComponent(btnNewButton_7, GroupLayout.PREFERRED_SIZE, 127, GroupLayout.PREFERRED_SIZE)))
						.addComponent(btnNewButton_8, GroupLayout.PREFERRED_SIZE, 127, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblBot_2)
						.addComponent(lblBot))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnCheck)
								.addComponent(btnFold)
								.addComponent(btnRaise)
								.addComponent(btnCall))
							.addGap(48))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(textField_3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(56))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblJugador)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addComponent(button_1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(button, GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE))
							.addContainerGap())))
		);
		setLayout(groupLayout);
		background = new ImageIcon("tapete_poker_neopreno_azul.jpg").getImage();
		actualizarBotes(100,100,100,100);
	}
	@Override
    public void paint(Graphics g) {
        g.drawImage(background, 0, 0, getWidth(), getHeight(),
                        this);
 
        setOpaque(false);
        super.paint(g);
    }
	public static void mostrarCartas(Mano mano1, Mano mano2, Mano mano3, Mano mano4, int modo) {
		if (modo == 0) {
			button.setIcon(new ImageIcon(mano1.getUno().toString()+".png"));
			button_1.setIcon(new ImageIcon(mano1.getDos().toString()+".png"));
			btnNewButton.setIcon(null);
			btnNewButton_1.setIcon(null);
			button_2.setIcon(null);
			button_3.setIcon(null);
			btnNewButton_2.setIcon(null);
			btnNewButton_3.setIcon(null);
		} else {
			btnNewButton.setIcon(new ImageIcon(mano2.getUno().toString()+".png"));
			btnNewButton_1.setIcon(new ImageIcon(mano2.getDos().toString()+".png"));
			button_2.setIcon(new ImageIcon(mano3.getUno().toString()+".png"));
			button_3.setIcon(new ImageIcon(mano3.getDos().toString()+".png"));
			btnNewButton_2.setIcon(new ImageIcon(mano4.getUno().toString()+".png"));
			btnNewButton_3.setIcon(new ImageIcon(mano4.getDos().toString()+".png"));
		}
		
	}
	public static void mostrarBoard(Carta flop1, Carta flop2, Carta flop3, Carta river, Carta turn) {
		if (flop1 == null) {
			btnNewButton_4.setIcon(null);
			btnNewButton_5.setIcon(null);
			btnNewButton_6.setIcon(null);
			btnNewButton_7.setIcon(null);
			btnNewButton_8.setIcon(null);
		} else {
			btnNewButton_4.setIcon(new ImageIcon(flop1.toString()+".png"));
			btnNewButton_5.setIcon(new ImageIcon(flop2.toString()+".png"));
			btnNewButton_6.setIcon(new ImageIcon(flop3.toString()+".png"));
			btnNewButton_7.setIcon(new ImageIcon(turn.toString()+".png"));
			btnNewButton_8.setIcon(new ImageIcon(river.toString()+".png"));
		}
		
	}
	public static void actualizarBotes(int jugador, int bot1, int bot2, int bot3) {
		textField.setText(Integer.toString(bot1));
		textField_1.setText(Integer.toString(bot2));
		textField_2.setText(Integer.toString(bot3));
		textField_3.setText(Integer.toString(jugador));
		
	}
	public static boolean getDecision() {
		if (decision) {
			decision = false;
			return true;
		} else {
			return false;
		}
	}
	public static boolean getCall() {
		if (call) {
			call = false;
			return true;
		} else {
			return false;
		}
	}
	public static boolean getRaise() {
		if (raise) {
			raise = false;
			return true;
		} else {
			return false;
		}
	}
	public static boolean getCheck() {
		if (check) {
			check = false;
			return true;
		} else {
			return false;
		}
	}
	public static boolean getFold() {
		if (fold) {
			fold = false;
			return true;
		} else {
			return false;
		}
	}
}
