package hja;

import hja.model.Acciones;
import hja.model.Posicion;

public class Bot {

	public boolean decidirOpenRaise(String  mano, Posicion posicion){
		return Acciones.janda(mano,Acciones.oR(posicion));
	}
	
	public boolean decidirColdCall3bet(String mano,Posicion posicion0, Posicion posicion){
		return Acciones.janda(mano, Acciones.posVsPosCall3Bet(posicion0, posicion));
	}
	
	public boolean decidirCall3bet(String mano,Posicion posicion0, Posicion posicion){
		return Acciones.janda(mano, Acciones.posVsPosCall3Bet(posicion0, posicion));
	}
	
}
