package hja.GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import hja.model.Carta;
import hja.model.Comparador;
import hja.model.Mano;
import hja.model.ParserRango;

public class PanelCalculo extends JPanel {
	private JButton btnNewButton;
	private PanelSecundario matriz;
	private JButton btnCalcular;
	private JTextField textField_13;
	private JTextField textField_14;
	private JTextField textField_15;
	private JTextField textField_16;
	private JTextField textField_17;
	private JTextField textField_18;
	private JTextField textField_19;
	private JTextField textField_20;
	private JTextField textField_21;
	private JTextField textField_22;
	private JTextField textField_23;
	private JTextField textField_24;
	private JTextField textField_25;
	private JTextField textField_26;
	private JTextField textField_27;
	private JTextField textField_28;
	private JTextField textField_29;
	private JTextField textField_30;
	private JTextField textField_31;
	private JTextField textField_32;
	private JTextField textField_33;
	private JTextField textField_34;
	private JButton btnLimpiar;
	
	/**
	 * Create the panel.
	 */
	public PanelCalculo() {
		JLabel lblNewLabel_3 = new JLabel("Jugador 1 :");
		
		JLabel lblNewLabel_4 = new JLabel("Jugador 2 :");
		
		JLabel lblNewLabel_5 = new JLabel("Jugador 3 :");
		
		JLabel lblNewLabel_6 = new JLabel("Jugador 4 :");
		
		JLabel lblNewLabel_7 = new JLabel("Jugador 5 :");
		
		textField_13 = new JTextField();
		textField_13.setColumns(10);
		
		textField_14 = new JTextField();
		textField_14.setColumns(10);
		
		textField_15 = new JTextField();
		textField_15.setColumns(10);
		
		textField_16 = new JTextField();
		textField_16.setColumns(10);
		
		textField_17 = new JTextField();
		textField_17.setColumns(10);
		
		textField_18 = new JTextField();
		textField_18.setColumns(10);
		
		JLabel lblNewLabel_9 = new JLabel("Jugador 6 :");
		
		JLabel lblNewLabel_10 = new JLabel("Jugador 7 :");
		
		textField_19 = new JTextField();
		textField_19.setColumns(10);
		
		JLabel lblNewLabel_11 = new JLabel("Jugador 8 :");
		
		textField_20 = new JTextField();
		textField_20.setColumns(10);
		
		textField_21 = new JTextField();
		textField_21.setColumns(10);
		
		JLabel lblNewLabel_12 = new JLabel("Jugador 9 :");
		
		JLabel lblNewLabel_13 = new JLabel("Jugador 10 :");
		
		textField_22 = new JTextField();
		textField_22.setColumns(10);
		
		textField_23 = new JTextField();
		textField_23.setColumns(10);
		textField_23.setEditable(false);
		textField_24 = new JTextField();
		textField_24.setColumns(10);
		textField_24.setEditable(false);
		textField_25 = new JTextField();
		textField_25.setColumns(10);
		textField_25.setEditable(false);
		textField_26 = new JTextField();
		textField_26.setColumns(10);
		textField_26.setEditable(false);
		textField_27 = new JTextField();
		textField_27.setColumns(10);
		textField_27.setEditable(false);
		textField_28 = new JTextField();
		textField_28.setColumns(10);
		textField_28.setEditable(false);
		textField_29 = new JTextField();
		textField_29.setColumns(10);
		textField_29.setEditable(false);
		textField_30 = new JTextField();
		textField_30.setColumns(10);
		textField_30.setEditable(false);
		textField_31 = new JTextField();
		textField_31.setColumns(10);
		textField_31.setEditable(false);
		textField_32 = new JTextField();
		textField_32.setColumns(10);
		textField_32.setEditable(false);
		JButton btnCalcular = new JButton("Calcular");
		
		JLabel lblNewLabel_8 = new JLabel("Cartas quemadas :");
		
		textField_33 = new JTextField();
		textField_33.setColumns(10);
		
		JLabel lblNewLabel_14 = new JLabel("Board : ");
		
		textField_34 = new JTextField();
		textField_34.setColumns(10);
		
		JTextArea textArea = new JTextArea();
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				textField_13.setText("");
				textField_14.setText("");
				textField_15.setText("");
				textField_16.setText("");
				textField_17.setText("");
				textField_18.setText("");
				textField_19.setText("");
				textField_20.setText("");
				textField_21.setText("");
				textField_22.setText("");
				textField_23.setText("");
				textField_24.setText("");
				textField_25.setText("");
				textField_26.setText("");
				textField_27.setText("");
				textField_28.setText("");
				textField_29.setText("");
				textField_30.setText("");
				textField_31.setText("");
				textField_32.setText("");
				textField_33.setText("");
				textField_34.setText("");
			}
			
		});
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(textArea, GroupLayout.PREFERRED_SIZE, 453, GroupLayout.PREFERRED_SIZE)
							.addContainerGap())
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblNewLabel_10)
								.addComponent(lblNewLabel_12)
								.addComponent(lblNewLabel_11)
								.addComponent(lblNewLabel_13)
								.addComponent(lblNewLabel_7)
								.addComponent(lblNewLabel_9)
								.addComponent(lblNewLabel_6)
								.addComponent(lblNewLabel_5)
								.addComponent(lblNewLabel_4)
								.addComponent(lblNewLabel_3))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(textField_13, GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)
								.addComponent(textField_14, GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)
								.addComponent(textField_15, GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)
								.addComponent(textField_16, GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)
								.addComponent(textField_17, GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)
								.addComponent(textField_18, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)
								.addComponent(textField_22, GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)
								.addComponent(textField_21, GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)
								.addComponent(textField_20, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)
								.addComponent(textField_19, GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addComponent(textField_23, 0, 0, Short.MAX_VALUE)
								.addComponent(textField_24, 0, 0, Short.MAX_VALUE)
								.addComponent(textField_25, 0, 0, Short.MAX_VALUE)
								.addComponent(textField_26, 0, 0, Short.MAX_VALUE)
								.addComponent(textField_27, 0, 0, Short.MAX_VALUE)
								.addComponent(textField_28, 0, 0, Short.MAX_VALUE)
								.addComponent(textField_29, 0, 0, Short.MAX_VALUE)
								.addComponent(textField_30, 0, 0, Short.MAX_VALUE)
								.addComponent(textField_31, 0, 0, Short.MAX_VALUE)
								.addComponent(textField_32, GroupLayout.DEFAULT_SIZE, 48, Short.MAX_VALUE))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(btnLimpiar, GroupLayout.DEFAULT_SIZE, 116, Short.MAX_VALUE)
								.addComponent(btnCalcular, GroupLayout.DEFAULT_SIZE, 116, Short.MAX_VALUE)
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
									.addComponent(textField_34)
									.addComponent(lblNewLabel_14)
									.addComponent(textField_33)
									.addComponent(lblNewLabel_8)))
							.addGap(37))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(40)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_3)
						.addComponent(textField_13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_23, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel_8))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_4)
						.addComponent(textField_14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_24, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_33, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(textField_15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel_5)
						.addComponent(textField_25, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel_14))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(textField_16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel_6)
						.addComponent(textField_26, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_34, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
						.addGroup(groupLayout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(textField_17, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_7)
								.addComponent(textField_27, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblNewLabel_9)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
									.addComponent(textField_18, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(textField_28, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblNewLabel_10)
								.addComponent(textField_19, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(textField_29, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(textField_20, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_11)
								.addComponent(textField_30, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(15)
							.addComponent(btnCalcular, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(textField_21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_12)
								.addComponent(textField_31, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblNewLabel_13)
								.addComponent(textField_22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(textField_32, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(21)
							.addComponent(btnLimpiar)))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(textArea, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(16, Short.MAX_VALUE))
		);
		setLayout(groupLayout);
		matriz = null;
		JLabel lblJugador = new JLabel("Jugador 1 :");
		
		
		
		btnNewButton = new JButton("Introducir rango mediante matriz");
		btnNewButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				matriz = new PanelSecundario(new boolean[13][13]);
				matriz.setVisible(true);
				
			}
			
		});
		
		
		
		btnCalcular.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int jugadores = 0;
				if (!textField_13.getText().equalsIgnoreCase("")) jugadores++;
				if (!textField_14.getText().equalsIgnoreCase("")) jugadores++;
				if (!textField_15.getText().equalsIgnoreCase("")) jugadores++;
				if (!textField_16.getText().equalsIgnoreCase("")) jugadores++;
				if (!textField_17.getText().equalsIgnoreCase("")) jugadores++;
				if (!textField_18.getText().equalsIgnoreCase("")) jugadores++;
				if (!textField_19.getText().equalsIgnoreCase("")) jugadores++;
				if (!textField_20.getText().equalsIgnoreCase("")) jugadores++;
				if (!textField_21.getText().equalsIgnoreCase("")) jugadores++;
				if (!textField_22.getText().equalsIgnoreCase("")) jugadores++;
				boolean [][][] rango = new boolean[jugadores][13][13];
				Mano[][] manos = new Mano[jugadores][20];
				ArrayList<Mano> aux = new ArrayList<Mano>();
				boolean[][] aux2 = new boolean[13][13];
				int jugador = 0;
				
				if (!textField_13.getText().equalsIgnoreCase("")) {
					aux2 = ParserRango.parseadorRango(textField_13.getText(), aux, jugador);
					for (int i = 0; i < 13; i++) {
						for (int j = 0; j <13; j++) {
							rango[jugador][i][j] = aux2[i][j];
						}
					}
					for (int w = 0; w < aux.size(); w++) {
						manos[jugador][w] = aux.get(w);
					}
					jugador++;
					aux.clear();
					aux2 = new boolean[13][13];
				}
				if (!textField_14.getText().equalsIgnoreCase("")) {
					aux2 = ParserRango.parseadorRango(textField_14.getText(), aux, jugador);
					for (int i = 0; i < 13; i++) {
						for (int j = 0; j <13; j++) {
							rango[jugador][i][j] = aux2[i][j];
						}
					}
					for (int w = 0; w < aux.size(); w++) {
						manos[jugador][w] = aux.get(w);
					}
					jugador++;
					aux.clear();
					aux2 = new boolean[13][13];
				}
				if (!textField_15.getText().equalsIgnoreCase("")) {
					aux2 = ParserRango.parseadorRango(textField_15.getText(), aux, jugador);
					for (int i = 0; i < 13; i++) {
						for (int j = 0; j <13; j++) {
							rango[jugador][i][j] = aux2[i][j];
						}
					}
					for (int w = 0; w < aux.size(); w++) {
						manos[jugador][w] = aux.get(w);
					}
					jugador++;
					aux.clear();
					aux2 = new boolean[13][13];
				}
				if (!textField_16.getText().equalsIgnoreCase("")) {
					aux2 = ParserRango.parseadorRango(textField_16.getText(), aux, jugador);
					for (int i = 0; i < 13; i++) {
						for (int j = 0; j <13; j++) {
							rango[jugador][i][j] = aux2[i][j];
						}
					}
					for (int w = 0; w < aux.size(); w++) {
						manos[jugador][w] = aux.get(w);
					}
					jugador++;
					aux.clear();
					aux2 = new boolean[13][13];
				}
				if (!textField_17.getText().equalsIgnoreCase("")) {
					aux2 = ParserRango.parseadorRango(textField_17.getText(), aux, jugador);
					for (int i = 0; i < 13; i++) {
						for (int j = 0; j <13; j++) {
							rango[jugador][i][j] = aux2[i][j];
						}
					}
					for (int w = 0; w < aux.size(); w++) {
						manos[jugador][w] = aux.get(w);
					}
					jugador++;
					aux.clear();
					aux2 = new boolean[13][13];
				}
				if (!textField_18.getText().equalsIgnoreCase("")) {
					aux2 = ParserRango.parseadorRango(textField_18.getText(), aux, jugador);
					for (int i = 0; i < 13; i++) {
						for (int j = 0; j <13; j++) {
							rango[jugador][i][j] = aux2[i][j];
						}
					}
					for (int w = 0; w < aux.size(); w++) {
						manos[jugador][w] = aux.get(w);
					}
					jugador++;
					aux.clear();
					aux2 = new boolean[13][13];
				}
				if (!textField_19.getText().equalsIgnoreCase("")) {
					aux2 = ParserRango.parseadorRango(textField_19.getText(), aux, jugador);
					for (int i = 0; i < 13; i++) {
						for (int j = 0; j <13; j++) {
							rango[jugador][i][j] = aux2[i][j];
						}
					}
					for (int w = 0; w < aux.size(); w++) {
						manos[jugador][w] = aux.get(w);
					}
					jugador++;
					aux.clear();
					aux2 = new boolean[13][13];
				}
				if (!textField_20.getText().equalsIgnoreCase("")) {
					aux2 = ParserRango.parseadorRango(textField_20.getText(), aux, jugador);
					for (int i = 0; i < 13; i++) {
						for (int j = 0; j <13; j++) {
							rango[jugador][i][j] = aux2[i][j];
						}
					}
					for (int w = 0; w < aux.size(); w++) {
						manos[jugador][w] = aux.get(w);
					}
					jugador++;
					aux.clear();
					aux2 = new boolean[13][13];
				}
				if (!textField_21.getText().equalsIgnoreCase("")) {
					aux2 = ParserRango.parseadorRango(textField_21.getText(), aux, jugador);
					for (int i = 0; i < 13; i++) {
						for (int j = 0; j <13; j++) {
							rango[jugador][i][j] = aux2[i][j];
						}
					}
					for (int w = 0; w < aux.size(); w++) {
						manos[jugador][w] = aux.get(w);
					}
					jugador++;
					aux.clear();
					aux2 = new boolean[13][13];
				}
				if (!textField_22.getText().equalsIgnoreCase("")) {
					aux2 = ParserRango.parseadorRango(textField_22.getText(), aux, jugador);
					for (int i = 0; i < 13; i++) {
						for (int j = 0; j <13; j++) {
							rango[jugador][i][j] = aux2[i][j];
						}
					}
					for (int w = 0; w < aux.size(); w++) {
						manos[jugador][w] = aux.get(w);
					}
					aux.clear();
					aux2 = new boolean[13][13];
				}
				ArrayList<Carta> quemadas = ParserRango.parseadorCartas(textField_33.getText()), board = ParserRango.parseadorCartas(textField_34.getText());
				Comparador comparador = new Comparador(quemadas, board);
				float[] resultados = comparador.compara(rango, manos);
				DecimalFormat format = new DecimalFormat("0.00");
				jugador = 0;
				StringBuilder s= new StringBuilder(new String(""));
				for(int i=0;i<jugadores;i++){
					s.append(format.format(resultados[i]*100) + "\n");
				}
				s.append(comparador.getPruebas()+" simulaciones" + "\n");
				textArea.setText(s.toString());
				if (!textField_13.getText().equalsIgnoreCase("")) {
					textField_23.setText(format.format(resultados[jugador]*100));
					jugador++;
				} 
				if (!textField_14.getText().equalsIgnoreCase("")) {
					textField_24.setText(format.format(resultados[jugador]*100));
					jugador++;
				}
				if (!textField_15.getText().equalsIgnoreCase("")) {
					textField_25.setText(format.format(resultados[jugador]*100));
					jugador++;
				}
				if (!textField_16.getText().equalsIgnoreCase("")) {
					textField_26.setText(format.format(resultados[jugador]*100));
					jugador++;
				}
				if (!textField_17.getText().equalsIgnoreCase("")) {
					textField_27.setText(format.format(resultados[jugador]*100));
					jugador++;
				}
				if (!textField_18.getText().equalsIgnoreCase("")) {
					textField_28.setText(format.format(resultados[jugador]*100));
					jugador++;
				}
				if (!textField_19.getText().equalsIgnoreCase("")) {
					textField_29.setText(format.format(resultados[jugador]*100));
					jugador++;
				}
				if (!textField_20.getText().equalsIgnoreCase("")) {
					textField_30.setText(format.format(resultados[jugador]*100));
					jugador++;
				}
				if (!textField_21.getText().equalsIgnoreCase("")) {
					textField_31.setText(format.format(resultados[jugador]*100));
					jugador++;
				}
				if (!textField_22.getText().equalsIgnoreCase("")) {
					textField_32.setText(format.format(resultados[jugador]*100));
					jugador++;
				}
			}
			
		});
		
		
	}
}
