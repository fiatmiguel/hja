package hja.GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class BotonMano extends JButton {
	private int x;
	private int y;
	public BotonMano(int x, int y, PanelRangos panel) {
		this.x = x;
		this.y = y;
		this.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				panel.anyadirQuitarMano(x, y);
			}
			
		});
	}
}
