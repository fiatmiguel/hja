package hja.GUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.border.MatteBorder;

import hja.model.Valor;

public class PanelRangos extends JPanel {
	private BotonMano manos[][];
	private boolean rango[][];
	/**
	 * Create the panel.
	 */
	public PanelRangos() {
		setFont(new Font("Marlett", Font.PLAIN, 12));
		setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		setLayout(new GridLayout(13, 13));
		manos = new BotonMano[13][13];
		rango = new boolean[13][13];
		char carta1 = ' ', carta2 = ' ', suited = ' ';
		for (int i = 0; i < 13; i++) {
			switch(i) {
				case 0: carta1 = 'A'; break;
				case 1: carta1 = 'K'; break;
				case 2: carta1 = 'Q'; break;
				case 3: carta1 = 'J'; break;
				case 4: carta1 = 'T'; break;
				case 5: carta1 = '9'; break;
				case 6: carta1 = '8'; break;
				case 7: carta1 = '7'; break;
				case 8: carta1 = '6'; break;
				case 9: carta1 = '5'; break;
				case 10: carta1 = '4'; break;
				case 11: carta1 = '3'; break;
				case 12: carta1 = '2'; break;
				
			}
			for (int j = 0; j <13; j++) {
				switch(j) {
					case 0: carta2 = 'A'; break;
					case 1: carta2 = 'K'; break;
					case 2: carta2 = 'Q'; break;
					case 3: carta2 = 'J'; break;
					case 4: carta2 = 'T'; break;
					case 5: carta2 = '9'; break;
					case 6: carta2 = '8'; break;
					case 7: carta2 = '7'; break;
					case 8: carta2 = '6'; break;
					case 9: carta2 = '5'; break;
					case 10: carta2 = '4'; break;
					case 11: carta2 = '3'; break;
					case 12: carta2 = '2'; break;
					
				}
				
				if (i > j) {
					suited = 'o';
					
				} else if (j > i) {
					suited = 's';
				} 
				char c[];
				if (i == j) {
					c = new char[2];
					c[0] = carta1;
					c[1] = carta2;
				} else {
					if (i < j) {
						c = new char[3];
						c[0] = carta1;
						c[1] = carta2;
						c[2] = suited;
					} else {
						c = new char[3];
						c[0] = carta2;
						c[1] = carta1;
						c[2] = suited;
					}
					
				}
				
				
				
				manos[i][j] = new BotonMano(j,i, this);
				manos[i][j].setText(String.copyValueOf(c));
				if (i > j) {
					manos[i][j].setBackground(new Color(154,192,205));
				} else if (i < j) {
					manos[i][j].setBackground(new Color(50,205,50));
				} else {
					manos[i][j].setBackground(new Color(227,91,216));
				}
				manos[i][j].setFont(new Font("a", Font.BOLD, 10));
				this.add(manos[i][j]);
			}
		}
	}
	
	public String toString(){
		StringBuilder s = new StringBuilder(new String(""));
		Valor v=Valor.A;
		int cont=0;
		if (rango[0][0])
			cont++;	
		for(int i=12;i>=0;i--){//miramos las parejas
			if(i!=0 && rango[13-i][13-i]){//cuando i==0 solo tiene que imprimir las parejas restantes
				if(cont==0 && i!=0)
					v=Valor.values()[i];
				if (i!=0)
					cont++;
				if(i==0)
					i--;
			}else{
				if (cont>=1){
					s.append(v.toString());
					s.append(v.toString());
				}if (cont>1){
					s.append('-');
					s.append(Valor.values()[i+1]);
					s.append(Valor.values()[i+1]);
				}
				if (cont>0)
					s.append(',');
				cont=0;
			}
		}
		//suited
		v=Valor.K;
		cont=0;
		for(int i=12;i>=0;i--){//AxSuited
			if(i!=0 && rango[0][13-i]){//cuando i==0 solo tiene que imprimir lo restantes
				if(cont==0 && i!=0)
					v=Valor.values()[i];
				if (i!=0)
					cont++;
				if(i==0)
					i--;
			}else{
				if (cont>=1){
					s.append(Valor.A.toString());
					s.append(v.toString());
					s.append('s');
				}if (cont>1){
					s.append('-');
					s.append(Valor.A.toString());
					s.append(Valor.values()[i+1]);
					s.append('s');
				}
				if (cont>0)
					s.append(',');
				cont=0;
			}
		}
		
		for(int i=12;i>0;i--){
			cont=0;
			v=Valor.values()[i-1];
			for(int j=i-1;j>=0;j--){
				if(j!=0 && rango[13-i][13-j]  ){//cuando i==0 solo tiene que imprimir lo restantes
					if(cont==0 && i!=0)
						v=Valor.values()[j];
					if (j!=0)
						cont++;
					if(j==0)
						j--;
				}else{
					if (cont>=1){
						s.append(Valor.values()[i].toString());
						s.append(v.toString());
						s.append('s');
					}if (cont>1){
						s.append('-');
						s.append(Valor.values()[i].toString());
						s.append(Valor.values()[j+1].toString());
						s.append('s');
					}
					if (cont>0)
						s.append(',');
					cont=0;
				}
			}
			
		}
		
		//offsuited
		
		v=Valor.K;
		cont=0;
		for(int i=12;i>=0;i--){
			if(i!=0 && rango[13-i][0]  ){//cuando i==0 solo tiene que imprimir las parejas restantes
				if(cont==0 && i!=0)
					v=Valor.values()[i];
				if (i!=0)
					cont++;
				if(i==0)
					i--;
			}else{
				if (cont>=1){
					s.append(Valor.A.toString());
					s.append(v.toString());
					s.append('o');
				}if (cont>1){
					s.append('-');
					s.append(Valor.A.toString());
					s.append(Valor.values()[i+1]);
					s.append('o');
				}
				if (cont>0)
					s.append(',');
				cont=0;
			}
		}
		
		for(int i=12;i>0;i--){
			cont=0;
			v=Valor.values()[i-1];
			for(int j=i-1;j>=0;j--){
				if( j!=0  && rango[13-j][13-i] ){//cuando i==0 solo tiene que imprimir lo restantes
					if(cont==0 && j!=0)
						v=Valor.values()[j];
					if (j!=0)
						cont++;
					if(j==0)
						j--;
				}else{
					if (cont>=1){
						s.append(Valor.values()[i].toString());
						s.append(v.toString());
						s.append('o');
					}if (cont>1){
						s.append('-');
						s.append(Valor.values()[i].toString());
						s.append(Valor.values()[j+1].toString());
						s.append('o');
					}
					if (cont>0)
						s.append(',');
					cont=0;
				}
			}
			
		}
		
		return s.toString();
	}
	
	
	public void actualizarRango(boolean[][] rango) {
		this.rango = rango;
		actualizarManos();
	}
	private void actualizarManos() {
		for (int i = 0; i < 13; i++) {
			for (int j = 0; j < 13; j++) {
				if (this.rango[i][j]) manos[i][j].setBackground(Color.YELLOW);
				else {
					if (i > j) {
						manos[i][j].setBackground(new Color(154,192,205));
					} else if (i < j) {
						manos[i][j].setBackground(new Color(50,205,50));
					} else {
						manos[i][j].setBackground(new Color(227,91,216));
					}
				}
			}
		}
	}
	public void anyadirQuitarMano(int x, int y) {
		if (rango[y][x]) rango[y][x] = false;
		else rango[y][x] = true;
		actualizarManos();
	}

}
