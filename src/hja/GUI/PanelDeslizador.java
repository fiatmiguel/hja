package hja.GUI;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import hja.model.RangoToManos;

public class PanelDeslizador extends JPanel {
	private JSlider slider;
	private PanelRangos rangos;
	/**
	 * Create the panel.
	 */
	public PanelDeslizador() {
		setLayout(new BorderLayout(0, 0));
		rangos = new PanelRangos();
		slider = new JSlider();
		slider.setValue(0);
		slider.setPaintLabels(true);
		slider.setPaintTicks(true);
		slider.setBorder(new LineBorder(new Color(0, 0, 0)));
		slider.setMaximum(100);
		slider.setMinimum(0);
		slider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {
				int rango = slider.getValue();
				rangos.actualizarRango(RangoToManos.ToManos(rango, PanelPorcentaje.getList()));
				
			}
			
		});
		slider.setMajorTickSpacing(5);
		slider.setMinorTickSpacing(1);
		add(slider, BorderLayout.SOUTH);
		add(rangos, BorderLayout.CENTER);

	}
	public void limpiar() {
		rangos.actualizarRango(new boolean[13][13]);
	}
	public void actualizarRango(boolean[][] rango) {
		rangos.actualizarRango(rango);
	}
	public String toString() {
		System.out.println(rangos.toString());
		return rangos.toString();
	}

}
