package hja.GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;

public class PanelSecundario extends JFrame {

	private JPanel contentPane;
	private PanelPorcentaje porcentaje;
	private PanelDeslizador panelRangos;
	
	/**
	 * Create the frame.
	 */
	public PanelSecundario(boolean[][] rango) {
		porcentaje = new PanelPorcentaje(this);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 955, 476);
		contentPane = new JPanel();
		contentPane.setBorder(new CompoundBorder());
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		panelRangos = new PanelDeslizador();
		panelRangos.setFont(new Font("Marlett", Font.PLAIN, 12));
		tabbedPane.addTab("Preflop", panelRangos);
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		contentPane.add(porcentaje, BorderLayout.EAST);
		//actualizarRango(rango);
	}
	public void limpiar() {
		panelRangos.limpiar();
	}
	public void actualizarRango(boolean[][] rango) {
		panelRangos.actualizarRango(rango);
	}
	public void mostrarRango() {
		JOptionPane.showMessageDialog(null, panelRangos.toString(), "Rango", JOptionPane.INFORMATION_MESSAGE);
	}

}
