package hja.GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import hja.model.Acciones;
import hja.model.Posicion;

public class PanelJugada extends JPanel {
	private JTextField textField;
	private JLabel lblRango;
	private JLabel lblMano;
	private JLabel lblPosicion;
	private JLabel lblAccion;
	private JButton btnNewButton;
	private JComboBox comboBox;
	private JComboBox comboBox_1;
	private JComboBox comboBox_2;
	/**
	 * Create the panel.
	 */
	public PanelJugada() {
		
		lblRango = new JLabel("Rango :");
		
		lblMano = new JLabel("Mano :");
		
		lblPosicion = new JLabel("Posicion :");
		
		lblAccion = new JLabel("Accion :");
		
		btnNewButton = new JButton("Resultado");
		btnNewButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				boolean correcto = false;
				String par = new String(textField.getText()), nuevoPar = new String();
				if (par.equalsIgnoreCase("")) {
					JOptionPane.showMessageDialog(null, "Introduce una jugada valida", "Jugada", JOptionPane.INFORMATION_MESSAGE);
				} else {
					if (par.length() >= 4) {
						nuevoPar += par.charAt(0);
						nuevoPar += par.charAt(2);
						if (par.charAt(1) == par.charAt(3)) {
							nuevoPar += "s";
						} else {
							nuevoPar += "o";
						}
					} else {
						nuevoPar = par;
					}
					if (comboBox.getSelectedItem().equals("Will Ma")) {
						correcto = Acciones.willMa(nuevoPar, Acciones.charToPosicion(comboBox_1.getSelectedItem().toString()), comboBox_2.getSelectedItem().toString());
					} else {
						correcto = Acciones.janda(nuevoPar, 0);
					}
					if (correcto) {
						JOptionPane.showMessageDialog(null, "Jugada correcta", "Jugada", JOptionPane.INFORMATION_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(null, "Jugada incorrecta", "Jugada", JOptionPane.ERROR_MESSAGE);
					}
					textField.setText("");
				}
				
			}
			
		});
		
		comboBox = new JComboBox();
		comboBox.addItem("Will Ma");
		comboBox.addItem("M. Janda");
		
		textField = new JTextField();
		textField.setColumns(10);
		
		comboBox_1 = new JComboBox();
		comboBox_1.addItem(Posicion.CO);
		comboBox_1.addItem(Posicion.BTN);
		comboBox_1.addItem(Posicion.SB);
		
		comboBox_2 = new JComboBox();
		comboBox_2.addItem("OR");
		comboBox_2.addItem("Fold");
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblPosicion)
								.addComponent(lblAccion)
								.addComponent(lblRango)
								.addComponent(lblMano))
							.addGap(18)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addComponent(comboBox_2, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(comboBox_1, 0, 107, Short.MAX_VALUE)
								.addComponent(textField, GroupLayout.DEFAULT_SIZE, 163, Short.MAX_VALUE)
								.addComponent(comboBox, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(49)
							.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 351, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(50, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(30)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblRango)
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblMano)
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblPosicion)
						.addComponent(comboBox_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAccion)
						.addComponent(comboBox_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(52, Short.MAX_VALUE))
		);
		setLayout(groupLayout);

	}
}
